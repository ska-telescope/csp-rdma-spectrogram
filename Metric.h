// Copyright 2020 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * Metric.h
 * Andrew Ensor
 * C++ class that represents one metric for MetricGenerator
 * Each accumulation performed by accumulateMetric is presumed to provide all polarisation products and all baselines for only the current channel
 * and the current channel is presumed to be incremented by one each accumulation
 * Selected polarisation products (XX when not DUALPOLARISED or XX, XY, YX, YY when DUALPOLARISED) can be filtered for inclusion in the metric
 * Selected baselines can be filtered for including in the metric
 * Selected channel intervals [start,end) can be filtered for inclusion where all channels in each interval are aggregated together for the metric
 * The channel filer intervals are presumed to be disjoint and in increasing order of channels
 * Metric accumulations are stored and output in order of polarisations innermost, then baselines, then channel intervals outmost
 * There is one metric value output by resolveAndReturn for every combination of
 * each selected polarisation product, each baseline, and each channel interval
 */

#ifndef METRIC_H
#define METRIC_H

#include <cuComplex.h>
#include <cuda_fp16.h>
#include <limits> /* used for std::numeric_limits */

#include "Definitions.h"


/**********************************************************************
 * C++ class that represents one metric for MetricGenerator
 **********************************************************************/

class Metric
{

public:

    enum class OperationType {OPERATION_MEAN, OPERATION_MIN, OPERATION_MAX, OPERATION_VARIANCE};
    enum class OperandType {OPERAND_REAL, OPERAND_IMAG, OPERAND_AMP, OPERAND_PHASE};

    /**********************************************************************
     * Metric constructor
     **********************************************************************/
    Metric(uint32_t numBaselines, uint32_t numChannels, OperationType operation, OperandType operand);

    /**********************************************************************
     * Metric clearDeviceAccumulations to clear the accumulations on the device
     **********************************************************************/
    void clearDeviceAccumulations();

    /**********************************************************************
     * Metric init function to specify filters for the metric
     **********************************************************************/
    void init(uint32_t polarisationFilter[], uint polFilterLength,
        uint32_t baselineFilter[], uint blFilterLength,
        uint32_t channelFilter[][2], uint numChannelIntervals,
        int cudaBlockSize);

    /**********************************************************************
     * Metric accumulateMetric to extract operand from the visibilities, perform operation and accumulate
     * Note this method presumes all polarisation products and all baselines are provided for only the current channel
     * Note this presumes channelIntervalIndex is up to date for the current channel eg by first calling isAccumulatingChannel
     **********************************************************************/
    void accumulateMetric(void* dataPointer);

    /**********************************************************************
     * Metric resolveAndReturn to resolve metrics from the accumulations calculated in deviceData and return to host buffer
     **********************************************************************/
    void resolveAndReturn(float *outputData);

    /**********************************************************************
     * Metric getOperation returns the operation used for this metric
     **********************************************************************/
    OperationType getOperation();

    /**********************************************************************
     * Metric getOperand returns the operand used for this metric
     **********************************************************************/
    OperandType getOperand();

    /**********************************************************************
     * Metric getOperand returns the operand used for this metric as a string
     **********************************************************************/
    std::string getOperandString();

    /**********************************************************************
     * Metric getNumChannelIntervals returns the number of channel intervals used by metric
     **********************************************************************/
    uint32_t getNumChannelIntervals();

    /**********************************************************************
     * Metric getChannelFilter returns the specified start and end filter for a channel interval
     **********************************************************************/
    uint32_t getChannelFilter(uint32_t chIndex, uint32_t isEnd);

    /**********************************************************************
     * Metric getBlFilterLength returns the number of baseline filters used by metric
     **********************************************************************/
    uint32_t getBlFilterLength();

    /**********************************************************************
     * Metric getBaselineFilter returns the specified baseline used by matric
     **********************************************************************/
    uint32_t getBaselineFilter(uint32_t blIndex);

    /**********************************************************************
     * Metric getPolFilterLength returns the number of polarisation filters used by metric
     **********************************************************************/
    uint32_t getPolFilterLength();

    /**********************************************************************
     * Metric getPolarisationFilter returns the specified polarisation as a string used by metric
     **********************************************************************/
    std::string getPolarisationFilter(uint32_t polIndex);

    /**********************************************************************
     * Metric getNumOutputMetrics returns the number of metrics that will be output
     **********************************************************************/
    uint32_t getNumOutputMetrics();

    /**********************************************************************
     * Metric getDeviceAccumulations returns the float buffer used to store metric accumulations on the device
     **********************************************************************/
    float *getDeviceAccumulations();

    /**********************************************************************
     * Metric getDevicePolFilter returns the uint32_t buffer containing the polarisation filter on the device
     **********************************************************************/
    uint32_t *getDevicePolFilter();

    /**********************************************************************
     * Metric getDeviceBlFilter returns the uint32_t buffer containing the baseline filter on the device
     **********************************************************************/
    uint32_t *getDeviceBlFilter();

    /**********************************************************************
     * Metric isAccumulatingChannel returns whether the specified channel falls within channel filter
     * Note this function uses and modifies field channelIntervalIndex to help its efficiency
     **********************************************************************/
    bool isAccumulatingChannel(uint32_t channel);

    /**********************************************************************
     * Metric cleanup to free the memory buffer
     **********************************************************************/
    void cleanup();

private:
    uint numBaselines;
    uint numChannels;
    OperationType operation;
    OperandType operand;

    uint32_t *polarisationFilter;
    uint polFilterLength;
    uint32_t *baselineFilter;
    uint blFilterLength;
    uint32_t (*channelFilter)[2];
    uint numChannelIntervals;
    uint channelIntervalIndex; /* current index in the channelFilter array which is maintained by calling isAccumulatingChannel */

    uint32_t numOutputMetrics;

    int cudaBlockSize; // cuda kernel block size
    int cudaAccumGridSize; // cuda kernel grid size used for accumulations of this metric
    int cudaResolveGridSize; // cuda kernel grid size used for resolving the metric after accumulations

    float *deviceAccumulations; // device data block used on GPU to store the accumulated sums, etc
    uint *accumulationCountInInterval; // count of number of accumulations currently used in deviceAccumulations for each interval 
    uint *deviceAccumulationCountInInterval; // device temporary copy of accumulationCountInInterval used by resolveAndReturn
    uint32_t *devicePolFilter; // device copy of the polarisation filter
    uint32_t *deviceBlFilter; // device copy of the baseline filter

};

#endif
