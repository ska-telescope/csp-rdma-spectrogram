// Copyright 2020 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * Metric.cu
 * Andrew Ensor
 * C++ class that represents one metric for MetricGenerator
 * Each accumulation performed by accumulateMetric is presumed to provide all polarisation products and all baselines for only the current channel
 * and the current channel is presumed to be incremented by one each accumulation
 * Selected polarisation products (XX when not DUALPOLARISED or XX, XY, YX, YY when DUALPOLARISED) can be filtered for inclusion in the metric
 * Selected baselines can be filtered for including in the metric
 * Selected channel intervals [start,end) can be filtered for inclusion where all channels in each interval are aggregated together for the metric
 * The channel filer intervals are presumed to be disjoint and in increasing order of channels
 * Metric accumulations are stored and output in order of polarisations innermost, then baselines, then channel intervals outmost
 * There is one metric value output by resolveAndReturn for every combination of
 * each selected polarisation product, each baseline, and each channel interval
 */

#include <iomanip> // used for type uint32_t

#include "Metric.h"

/**********************************************************************
 * clearAccumulationsOnGPU Cuda kernel that resets the accumulations on device
 * when using OPERATION_MIN or OPERATION_MAX as can't simply reset to 0
 **********************************************************************/
__global__ void clearAccumulationsOnGPU(float *deviceAccumulations,
    uint32_t numOutputMetrics, float resetValue)
{
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    if (i < numOutputMetrics)
    {
        deviceAccumulations[i] = resetValue;
    }
}


/**********************************************************************
 * accumulateOnGPU Cuda kernel that converts complex deviceData entries
 * to operand and perform operation on them to deviceAccumulations
 **********************************************************************/
#ifdef INPUT16BIT
    __global__ void accumulateOnGPU(half2 *deviceData, float *deviceAccumulations, uint32_t numOutputMetrics,
        uint channelBlockSize,
        Metric::OperationType operation, Metric::OperandType operand,
        uint32_t *devicePolFilter, uint polFilterLength, uint32_t *deviceBlFilter,
        uint channelIntervalIndex)
#else
    __global__ void accumulateOnGPU(cuFloatComplex *deviceData, float *deviceAccumulations, uint32_t numOutputMetrics,
        uint channelBlockSize,
        Metric::OperationType operation, Metric::OperandType operand,
        uint32_t *devicePolFilter, uint polFilterLength, uint32_t *deviceBlFilter,
        uint channelIntervalIndex)
#endif
{
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    if (i < channelBlockSize)
    {
        uint32_t polarisation = devicePolFilter[i % polFilterLength];
        uint32_t baseline = deviceBlFilter[i / polFilterLength];
        #ifdef INPUT16BIT
            #ifdef DUALPOLARISED
                cuFloatComplex complexValue = __half22float2(deviceData[baseline*4+polarisation]);
            #else
                cuFloatComplex complexValue = __half22float2(deviceData[baseline+polarisation]);
            #endif
        #else
            #ifdef DUALPOLARISED
                cuFloatComplex complexValue = deviceData[baseline*4+polarisation];
            #else
                cuFloatComplex complexValue = deviceData[baseline+polarisation];
            #endif
        #endif
        // obtain the appropriate operand
        float operandValue = 0;
        switch (operand)
        {
            case Metric::OperandType::OPERAND_REAL :
                operandValue = complexValue.x;
                break;
            case Metric::OperandType::OPERAND_IMAG :
                operandValue = complexValue.y;
                break;
            case Metric::OperandType::OPERAND_AMP :
                operandValue = cuCabsf(complexValue);
                break;
            case Metric::OperandType::OPERAND_PHASE :
                operandValue = atan2f(complexValue.y, complexValue.x);
                break;
        }

        switch (operation)
        {
            case Metric::OperationType::OPERATION_MEAN :
                deviceAccumulations[channelIntervalIndex*channelBlockSize+i] += operandValue;
                break;
            case Metric::OperationType::OPERATION_MIN :
                deviceAccumulations[channelIntervalIndex*channelBlockSize+i]
                    = min(deviceAccumulations[channelIntervalIndex*channelBlockSize+i], operandValue);
                break;
            case Metric::OperationType::OPERATION_MAX :
                deviceAccumulations[channelIntervalIndex*channelBlockSize+i]
                    = max(deviceAccumulations[channelIntervalIndex*channelBlockSize+i], operandValue);
                break;
            case Metric::OperationType::OPERATION_VARIANCE :
                deviceAccumulations[channelIntervalIndex*channelBlockSize+i] += operandValue;
                deviceAccumulations[numOutputMetrics+channelIntervalIndex*channelBlockSize+i] += operandValue*operandValue;
                break;
        }
    }
}


/**********************************************************************
 * resolveOnGPU Cuda kernel that resolves the metrics from the calculated deviceAccumulations
 * for operations OPERATION_MEAN and OPERATION_VARIANCE that require additional calculations
 **********************************************************************/
__global__ void resolveOnGPU(float *deviceAccumulations, uint32_t numOutputMetrics,
    uint *deviceAccumulationCountInInterval, uint channelBlockSize, Metric::OperationType operation)
{
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    if (i < numOutputMetrics)
    {
        uint numBlocksAccumulated = deviceAccumulationCountInInterval[i/channelBlockSize];
        if (operation == Metric::OperationType::OPERATION_MEAN)
        {
            deviceAccumulations[i] /= numBlocksAccumulated;
        }
        else if (operation == Metric::OperationType::OPERATION_VARIANCE)
        {
            deviceAccumulations[i] = (deviceAccumulations[numOutputMetrics+i]*numBlocksAccumulated - deviceAccumulations[i]*deviceAccumulations[i])
                / (numBlocksAccumulated*numBlocksAccumulated);
        }
    }
}


/**********************************************************************
 * Metric constructor
 **********************************************************************/
Metric::Metric(uint32_t numBaselines, uint32_t numChannels, Metric::OperationType operation, Metric::OperandType operand)
{
    this->numBaselines = numBaselines;
    this->numChannels = numChannels;
    this->operation = operation;
    this->operand = operand;
}


/**********************************************************************
 * Metric clearDeviceAccumulations to clear the accumulations on the device
 **********************************************************************/
void Metric::clearDeviceAccumulations()
{
    size_t numAccumulationBytes = this->numOutputMetrics*sizeof(float);
    if (this->getOperation()==OperationType::OPERATION_MEAN || this->getOperation()==OperationType::OPERATION_VARIANCE)
    {
        // clear the accumulations to zero taking care to also clear sum of square accumulations when using variance
        if (this->operation == Metric::OperationType::OPERATION_VARIANCE)
        {
            // double the memory is required to store both sum and sum of squares
            numAccumulationBytes *= 2;
        }
        cudaMemset(this->deviceAccumulations, 0, numAccumulationBytes); /* simple byte reset to 0 is sufficient */
    }
    /* otherwise use a CUDA kernel to clear the accumulations in case of OPERATION_MIN or OPERATION_MAX */
    else if (this->getOperation() == OperationType::OPERATION_MIN)
    {
        clearAccumulationsOnGPU<<<this->cudaResolveGridSize, this->cudaBlockSize>>>(this->getDeviceAccumulations(),
            this->numOutputMetrics, std::numeric_limits<float>::max());
    }
    else if (this->getOperation() == OperationType::OPERATION_MAX)
    {
        clearAccumulationsOnGPU<<<this->cudaResolveGridSize, this->cudaBlockSize>>>(this->getDeviceAccumulations(),
            this->numOutputMetrics, -std::numeric_limits<float>::max()); /* note this presumes IEEE754 floats with separate sign bit */
    }
    size_t numAccumCountBytes = this->numChannelIntervals*sizeof(uint);
    memset(this->accumulationCountInInterval, 0, numAccumCountBytes);
    /* note no need to also clear deviceAccumulationCountInInterval as it only temporarily holds accumulation counts */
}


/**********************************************************************
 * Metric init function to specify filters for the metric
 **********************************************************************/
void Metric::init(uint32_t polarisationFilter[], uint polFilterLength,
    uint32_t baselineFilter[], uint blFilterLength,
    uint32_t channelFilter[][2], uint numChannelIntervals,
    int cudaBlockSize)
{
    this->polarisationFilter = polarisationFilter;
    this->polFilterLength = polFilterLength;
    this->baselineFilter = baselineFilter;
    this->blFilterLength = blFilterLength;
    this->channelFilter = channelFilter;
    this->numChannelIntervals = numChannelIntervals;
    this->channelIntervalIndex = 0;

    /* calculate numOutputMetrics */
    this->numOutputMetrics = polFilterLength * blFilterLength * numChannelIntervals;

    this->cudaBlockSize = cudaBlockSize;
    this->cudaAccumGridSize = (polFilterLength*blFilterLength + cudaBlockSize - 1)/cudaBlockSize;   
    this->cudaResolveGridSize = (this->numOutputMetrics + cudaBlockSize - 1)/cudaBlockSize;   

    // allocate the buffer for the arithmetic sums (or min, max, ...) on the GPU device
    size_t numAccumulationBytes = this->numOutputMetrics*sizeof(float);
    if (this->operation == Metric::OperationType::OPERATION_VARIANCE)
    {
        // double the memory is required to store both sum and sum of squares
        numAccumulationBytes *= 2;
    }
    cudaMalloc((void**)&this->deviceAccumulations, numAccumulationBytes);

    // allocate the host and device side buffers to hold accumulation counts
    size_t numAccumCountBytes = this->numChannelIntervals*sizeof(uint);
    cudaHostAlloc((void**)&this->accumulationCountInInterval, numAccumCountBytes, 0);
    cudaMalloc((void**)&this->deviceAccumulationCountInInterval, numAccumCountBytes);

    this->clearDeviceAccumulations();

    /* copy the polarisation and baseline filters to the device where they get used */
    cudaMalloc((void**)&this->devicePolFilter, polFilterLength*sizeof(uint32_t));
    cudaMemcpy(this->devicePolFilter, polarisationFilter, polFilterLength*sizeof(uint32_t), cudaMemcpyHostToDevice);
    cudaMalloc((void**)&this->deviceBlFilter, blFilterLength*sizeof(uint32_t));
    cudaMemcpy(this->deviceBlFilter, baselineFilter, blFilterLength*sizeof(uint32_t), cudaMemcpyHostToDevice);
}


/**********************************************************************
 * Metric accumulateMetric to extract operand from the visibilities, perform operation and accumulate
 * Note this method presumes all polarisation products and all baselines are provided for only the current channel
 * Note this presumes channelIntervalIndex is up to date for the current channel eg by first calling isAccumulatingChannel
 **********************************************************************/
void Metric::accumulateMetric(void* dataPointer)
{
    #ifdef INPUT16BIT
        half2 *visibilityBlock = (half2 *)dataPointer;
    #else
        cuFloatComplex *visibilityBlock = (cuFloatComplex *)dataPointer;
    #endif

    // perform the accumulations of visibilities using FP32 summands
    accumulateOnGPU<<<this->cudaAccumGridSize, this->cudaBlockSize>>>(visibilityBlock,
        this->deviceAccumulations, this->getNumOutputMetrics(), this->polFilterLength*this->blFilterLength,
        operation, operand,
        this->devicePolFilter, this->polFilterLength, this->deviceBlFilter,
        this->channelIntervalIndex);
    this->accumulationCountInInterval[channelIntervalIndex]++;
}


/**********************************************************************
 * Metric resolveAndReturn to resolve metrics from the accumulations calculated in deviceData and return to host buffer
 **********************************************************************/
void Metric::resolveAndReturn(float *outputData)
{
    if (this->getOperation() == OperationType::OPERATION_MEAN || this->getOperation() == OperationType::OPERATION_VARIANCE)
    {
        // transfer the accumulation counts to the device
        size_t numAccumCountBytes = this->numChannelIntervals*sizeof(uint);
        cudaMemcpy(this->deviceAccumulationCountInInterval, this->accumulationCountInInterval,
            numAccumCountBytes, cudaMemcpyHostToDevice);
        // perform the averaging of the FP32 summands
        resolveOnGPU<<<this->cudaResolveGridSize, this->cudaBlockSize>>>(this->getDeviceAccumulations(),
            this->getNumOutputMetrics(), this->deviceAccumulationCountInInterval, this->polFilterLength*this->blFilterLength,
            this->getOperation());
    }
    size_t numAccumulationBytes = this->numOutputMetrics*sizeof(float);
    cudaMemcpy(outputData, this->deviceAccumulations, numAccumulationBytes, cudaMemcpyDeviceToHost);
    this->clearDeviceAccumulations();
}


/**********************************************************************
 * Metric getOperation returns the operation used for this metric
 **********************************************************************/
Metric::OperationType Metric::getOperation()
{
    return operation;
}


/**********************************************************************
 * Metric getOperand returns the operand used for this metric
 **********************************************************************/
Metric::OperandType Metric::getOperand()
{
    return operand;
}


/**********************************************************************
 * Metric getOperand returns the operand used for this metric as a string
 **********************************************************************/
std::string Metric::getOperandString()
{
    switch (Metric::getOperand())
    {
        case Metric::OperandType::OPERAND_REAL :
            return "real";
        case Metric::OperandType::OPERAND_IMAG :
            return "imag";
        case Metric::OperandType::OPERAND_AMP :
            return "amp";
        case Metric::OperandType::OPERAND_PHASE :
            return "phase";
    }
    return "unknown";
}


/**********************************************************************
 * Metric getNumChannelIntervals returns the number of channel intervals used by metric
 **********************************************************************/
uint32_t Metric::getNumChannelIntervals()
{
    return numChannelIntervals;
}


/**********************************************************************
 * Metric getChannelFilter returns the specified start and end filter for a channel interval
 **********************************************************************/
uint32_t Metric::getChannelFilter(uint32_t chIndex, uint32_t isEnd)
{
    return this->channelFilter[chIndex][isEnd];
}


/**********************************************************************
 * Metric getBlFilterLength returns the number of baseline filters used by metric
 **********************************************************************/
uint32_t Metric::getBlFilterLength()
{
    return blFilterLength;
}


/**********************************************************************
 * Metric getBaselineFilter returns the specified baseline used by matric
 **********************************************************************/
uint32_t Metric::getBaselineFilter(uint32_t blIndex)
{
    return this->baselineFilter[blIndex];
}


/**********************************************************************
 * Metric getPolFilterLength returns the number of polarisation filters used by metric
 **********************************************************************/
uint32_t Metric::getPolFilterLength()
{
    return polFilterLength;
}


/**********************************************************************
 * Metric getPolarisationFilter returns the specified polarisation as a string used by metric
 **********************************************************************/
std::string Metric::getPolarisationFilter(uint32_t polIndex)
{
    switch (this->polarisationFilter[polIndex])
    {
        case 0 :
            return "XX";
        case 1 :
            return "XY";
        case 2 :
            return "YX";
        case 3 :
            return "YY";
    }
    return "unknown";
}


/**********************************************************************
 * Metric getNumOutputMetrics returns the number of metrics that will be output
 **********************************************************************/
uint32_t Metric::getNumOutputMetrics()
{
    return numOutputMetrics;
}


/**********************************************************************
 * Metric getDeviceAccumulations returns the float buffer used to store metric accumulations on the device
 **********************************************************************/
float *Metric::getDeviceAccumulations()
{
    return deviceAccumulations;
}


/**********************************************************************
 * Metric getDevicePolFilter returns the uint32_t buffer containing the polarisation filter on the device
 **********************************************************************/
uint32_t *Metric::getDevicePolFilter()
{
    return devicePolFilter;
}


/**********************************************************************
 * Metric getDeviceBlFilter returns the uint32_t buffer containing the baseline filter on the device
 **********************************************************************/
uint32_t *Metric::getDeviceBlFilter()
{
    return deviceBlFilter;
}


/**********************************************************************
 * Metric isAccumulatingChannel returns whether the specified channel falls within channel filter
 * Note this function uses and modifies field channelIntervalIndex to help its efficiency
 **********************************************************************/
bool Metric::isAccumulatingChannel(uint32_t channel)
{
    /* advance channelIntervalIndex so that the incrementing channel will next fall within the filter interval at channelIntervalIndex */
    if (channel < channelFilter[0][1])
    {
        channelIntervalIndex = 0;
    }
    while (channel >= channelFilter[channelIntervalIndex][1])
    {
        channelIntervalIndex++;
        if (channelIntervalIndex >= this->numChannelIntervals)
        {
            channelIntervalIndex = 0;
            break; /* break from while loop */
        }
    }
    return channel >= channelFilter[channelIntervalIndex][0] && channel < channelFilter[channelIntervalIndex][1];
}


/**********************************************************************
 * Metric cleanup to free the memory buffer
 **********************************************************************/
void Metric::cleanup()
{
    cudaFree(this->deviceBlFilter);
    cudaFree(this->devicePolFilter);
    cudaFree(this->deviceAccumulationCountInInterval);
    cudaFreeHost(this->accumulationCountInInterval);
    cudaFree(this->deviceAccumulations);
}


