# CSP RDMA Spectrogram

## Overview

C++/Cuda project for producing spectrogram power data for time-based complex FP16 or FP32 input data.
The input data can be specified as either floating point (FP16 or float type) or integral (short or int) and either in single or double polarisation.

---

## Software and hardware requirements

### Code library requirements

Cuda installed with nvcc compiler (if want to build project).
libcurl library installed for handling HTTP POST requests (note Ubuntu default installation placement of libcurl library in /usr/lib/x86_64-linux-gnu/ should really be up one folder).
The project makes the HTTP POST request using pthreads to allow the main thread to focus on calling the Cuda kernels (note that currently outstanding POST requests continue after the main thread is completed, in which case any pthread logging may have undefined behaviour).

### InfluxDB requirements

The current implementation uses InfluxDB for storing the metrics in a time series database. InfluxDB can be installed as a Docker Image, for Mac OS X, or Linux following instructions at [InfluxDB Installation](https://portal.influxdata.com/downloads/). Older versions such as 1.8.3 can also be installed on Windows following the instructions found here: [Windows InfluxDB Installation](https://devconnected.com/how-to-install-influxdb-on-windows-in-2019/)

Once installed an InfluxDB database should be created (named for example �gleam�). From the command line:
```bash
$ influx -precision rfc3389
$ CREATE DATABASE spectrogram
```
No database schema need be specified when using InfluxDB. 

### Grafana requirements

The metrics stored in InfluxDB can be visualised using any visualisation tool, such as Grafana which is available from [Grafana Installation](https://grafana.com/grafana/download) for Linux, Windows, Mac, or ARM, or as a Docker container.
To allow the user to choose auto-refresh intervals for the dashboard lower than the default 5s minimum, follow the [Configuration Instructions](https://grafana.com/docs/grafana/latest/administration/configuration/) for specifying min_refresh_interval, restart Grafana and then within the dashboard settings select the auto=refresh time options.

### RDMA requirements

The ethernet adapter card on the sender and receiver need to both be configured for RDMA, such as RoCE V2. For a ConnectX-4/4LX/5 adapter cards (which may show as two RDMA device each with a single RDMA port 1) recommendations are
available at https://community.mellanox.com/s/article/recommended-network-configuration-examples-for-roce-deployment#jive_content_id_Recommended_Configurations. For an older ConnectX-3Pro card (which may show as RDMA device "mlx_4" with RDMA ports 1 and 2) there are some archived instructions at https://community.mellanox.com/s/article/howto-configure-roce-v2-for-connectx-3-pro-using-mellanox-switchx-switches.

Then the instructions at https://www.rdmamojo.com/2014/11/08/working-rdma-ubuntu/ were followed to install the RDMA packages for Ubuntu (some packages such as libdapl2, rds-tools and libibcommon1 were non longer available, and libopensm5 was replaced with libopensm5a). Firmware updates for the card and up to date packages are described at
https://www.mellanox.com/pdf/prod_software/Ubuntu_18_04_Inbox_Driver_User_Manual.pdf.

---

## Building the project

The project has only C++ and CUDA code, and is very simple to build once the correct libraries are installed,
including either the librdmaapi.so shared library or librdmaapi.a static library (see https://gitlab.com/ska-telescope/rdma-data-transport):
```bash
nvcc FFTWindow.cu Spectrogram.cu -lrdmaapi -lrdmacm -libverbs -lcurl -lpthread -lcufft -lm -arch=sm_60 -Xptxas -O3 -o spectrogram
```
which includes some common compiler optimisations.
The -arch flag is used to ensure Cuda API support for half2, but earlier architectures can be accommodated with minor code changes provided half to float operations are supported.
Note the project must be rebuilt whenever any combination of the Definitions.h compiler directive options DUALPOLARISED, INPUT16BIT, INPUTINTEGER are commented or uncommented.

The GitLab repository also includes a sample C++ RDMA sender SimulatedSignalGenerator.cpp for testing the spectrogram
as demonstrated in the SKA DP System Demo 8.6.
It requires the half-2.1.0 C++ floating-point library for FP16 support available from http://half.sourceforge.net/
(placed in a folder such as ../half-2.1.0/include/half.hpp) and can be built using:
```bash
g++ SimulatedSignalGenerator.cpp -lrdmaapi -lrdmacm -libverbs -lcurl -lpthread -lm -O3 -o simulatedsignalgenerator
```

CIPA have also provided a python script CrossPowerTester.py which generates a series of datafiles for testing the cross power.
After running the script:
```bash
python3 CrossPowerTester.py --flags 4000
```
a large number of dly, q2u, u2v files are generated. Once generated, these can be sent via RDMA using the RDMAapi:
```bash
./receive -m65536 -b4000 -fu2v -d50 -s
```
(using a suitable delay), and received using the spectrogram:
```bash
./spectrogram -m65536 -b4000 -v20
```

---

## Running the project

The project has various command line options:

Many of the command line options are common to the rdma-data-transport project:
* -l to specify the log level output to the console using RFC5424 (either 0=LOG_EMERG, 1=LOG_ALERT, 2=LOG_CRIT, 3=LOG_ERR, 4=LOG_WARNING, 5=LOG_NOTICE, 6=LOG_INFO, 7=LOG_DEBUG, default is LOG_NOTICE).
* -m to specify the individual message size in bytes (default is 65536).
* -b to specify how many memory region blocks to allocate (default is 1).
* -c to specify how many contiguous messages to have in each memory region, to help encourage utilisation of transparent huge pages and reduce the number of completion events signalled by the sender (default is 1). Each memory region is allocated to hold this number of messages.
* -t to specify the total number of message intended to send or receive (default is the total number of messages across all memory regions). Note that this should be a multiple of the number of contiguous messages as all the messages in a memory region are sent/received as a linked list of work requests for efficiency. If the total is greater than the number of messages on the sender or receiver it simply loops back to repeat earlier work requests. Note the receive will block until it successfully receives the intended number of messages (so hang if it received any messages while it could not keep up with the sender and had its receive request queue emptied).
* -r to specify the RDMA device name (default is NULL to let the application itself find a suitable RDMA device).
* -p to specify the device port (default is 1). Note when using a sender with localhost that some RDMA devices offer a second RDMA port which can be used for testing on localhost without only local loopback.
* -g to specify a specific index in the GID table to use (default is to let the application try to find a GID that supports IPv6 or else fall back to IPv4). Note the choice of GID will determine which RDMA technology (such as version of RoCE) will be utilised.
* -x to specify an exchange identifier filename for exchanging PSN,QPN,GID,LID identifiers rather than via stdio (note .send or .recv suffix gets appended to filename which should be on shared file system accessible to both sender and receiver).
* -o to specify an IP output address to which InfluxDB metrics should be posted via HTTP POST requests.
Each message is currently treated as a single input (time-based) data block to be processed via cuFFT.

It also has some command line options specific to this project:
* -v to specify number of messages over which to average before they get posted.
* -n to specify a name for the measurement to use in InfluxDB
* -w to specify the window to apply to the input data before applying the FFT (either 0=NONE, 1=BLACKMAN, 2=DOLPH_CHEBYSHEV, 3=FLATTOP_SR785, 4=FLATTOP_HFT70, 5=FLATTOP_HFT248D, 6=HANNING, 7=KAISER). Note all windows are L0-normalised so their central peak is 1.0.
* -s to specify the start channel (inclusive in range) to output (default is 0).
* -e to specify the end channel (exclusive in range) to output (default is the smaller of the message size and 512).

```bash
./spectrogram [-l log level 0..6] [-m message size in bytes] [-b num memory blocks] [-c num contig messages per block] [-t total num messages] [-r RDMA device name] [-p device port] [-g gid index] [-x exchange identifier filename] [-o metric url] [-v metric averaging] [-n output name] [-w window 0..6] [s start channel] [e end channel]
```

---

## Visibility Metric Generator

The project also includes a prototype visibility metric generator MetricGenerator.cu that receives visibilities for one channel in an RDMA message, either with one or four polarisation products.
It allows specific polarisation products and baselines to be filtered for inclusion in the metrics, and channel interval filters to aggregate across channels.
The metric generator allows mean, minimum, maximum and variance to be calculated as metrics for either the real or imaginary component of the visibility or instead its amplitude or phase.
Currently the metric generator supports only a single metric and has the choice of filters, operands and operators hard-coded (TODO: make these configurable).
The sample C++ RDMA sender SimulatedSignalGenerator.cpp can be used for testing the metric generator (TODO: undertake some careful correctness testing).

---