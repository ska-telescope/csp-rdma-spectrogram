#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import argparse

def get_data(size, Q, U, V):

	cov = np.asarray([[1+Q,   0,   U,   V],
		              [0,   1+Q,  -V,   U],
		              [U,    -V, 1-Q,   0],
		              [V,     U,   0, 1-Q]])

	mean = np.zeros(4)

	x = np.random.multivariate_normal(mean, cov, size, check_valid='raise')

	return x[:,0]+1.0j*x[:,1], x[:,2]+1.0j*x[:,3]


if __name__ == '__main__':


	parser = argparse.ArgumentParser(description='Process some integers.')
	parser.add_argument('--frames', default=200*20,   type=int, help='Number of frames per integrations.')
	parser.add_argument('-N',       default=16384, type=int, help='FFT Length')
	args = parser.parse_args()

	frames = args.frames
	fftlen = args.N

	# Simulate swapping power between Q and U
	Q = np.linspace(-0.5, 0.5, int(frames/2))
	Q = np.append(Q, Q[::-1])
	U = np.roll(Q, int(frames/4))

	for i in np.arange(len(Q)):
		x, y = get_data(fftlen, Q[i], U[i], 0)
		out = np.zeros(4*fftlen)
		out[0::4] = np.real(x)
		out[1::4] = np.imag(x)
		out[2::4] = np.real(y)
		out[3::4] = np.imag(y)
		out = np.round(out*2**10)
		out.astype(np.int16).tofile('q2u.%d' %(i))

	# Swap power between U and V
	U = np.linspace(-0.5, 0.5, int(frames/2))
	U = np.append(U, U[::-1])
	V = np.roll(U, int(frames/4))

	for i in np.arange(len(Q)):
		x, y = get_data(fftlen, 0, U[i], V[i])
		out = np.zeros(4*fftlen)
		out[0::4] = np.real(x)
		out[1::4] = np.imag(x)
		out[2::4] = np.real(y)
		out[3::4] = np.imag(y)
		out = np.round(out*2**10)
		out.astype(np.int16).tofile('u2v.%d' %(i))		

	# Simulate a phase ramp caused by delay
	offset = np.linspace(0, 2, int(frames/2))
	offset = np.append(offset, offset[::-1])
	for i in np.arange(len(offset)):
		x, y = get_data(fftlen+2, 0, 0.5, 0)
		y = np.interp(np.arange(len(y)-2)+offset[i], np.arange(len(y)), y)
		x = x[1:-1]
		out = np.zeros(4*fftlen)
		out[0::4] = np.real(x)
		out[1::4] = np.imag(x)
		out[2::4] = np.real(y)
		out[3::4] = np.imag(y)
		out = np.round(out*2**10)
		out.astype(np.int16).tofile('dly.%d' %(i))


