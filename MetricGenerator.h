// Copyright 2020 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * MetricGenerator.h
 * Andrew Ensor
 * C++/CUDA program for generating metrics for complex FP16 visibilities received in memory blocks
 * Each memory block is assumed to store polarisations innermost together, then baselines consecutively for a single channel
 * Consecutive memory blocks are assumed to be for consecutive channels, wrapping around to repeat again from channel 0
 * Metric averaging is counted for each iteration across all the channels
 */

#ifndef METRICGENERATOR_H
#define METRICGENERATOR_H

#include <chrono>
#include <iomanip>
#include <iostream>
#include <string>
#include <sstream>
#include <cuComplex.h>
#include <cuda_fp16.h>
#include <curl/curl.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h> // used for getopt and optarg in parseCommandLineArguments

#include "Definitions.h"
#include "Metric.h"

// import the C-based RDMA api
#ifdef __cplusplus
    extern "C"
    {
#endif
#include <RDMAapi.h>
#ifdef __cplusplus
    }
#endif


/**********************************************************************
 * C++/CUDA program for generating metrics for complex FP16 visibilities received in memory blocks
 **********************************************************************/

class MetricGenerator
{

public:

    /**********************************************************************
     * Returns the Cuda status and logs any synch or asynch errors
     **********************************************************************/
    cudaError_t checkCudaStatus();

    /**********************************************************************
     * MetricGenerator constructor
     **********************************************************************/
    MetricGenerator(std::string databaseURL, std::string outputName);

    /**********************************************************************
     * MetricGenerator init to set up working data buffers
     **********************************************************************/
    void init(uint numInputVisibilities, uint32_t numBaselines, uint32_t numChannels);

    /**********************************************************************
     * MetricGenerator transferVisibilities to transfer input block and potentially convert to float values
     **********************************************************************/
    void transferVisibilities(void* dataPointer);

    /**********************************************************************
     * MetricGenerator getNumInputVisibilities returns the number of complex data values per block
     **********************************************************************/
    inline int getNumInputVisibilities();

    /**********************************************************************
     * MetricGenerator getCudaBlockSize returns the CUDA block size
     **********************************************************************/
    int getCudaBlockSize();

    /**********************************************************************
     * MetricGenerator getDeviceData returns a pointer to the visibility data currently on device
     **********************************************************************/
    #ifdef INPUT16BIT
        half2 *getDeviceData();
    #else
        cuFloatComplex *getDeviceData();
    #endif

    /**********************************************************************
     * MetricGenerator postDataToDatabase posts the data block to a database via an HTTP request  
     **********************************************************************/
    void postDataToDatabase(float *outputData, Metric metric);

    /**********************************************************************
     * MetricGenerator cleanupBuffers to free the memory buffers
     **********************************************************************/
    void cleanupBuffers();

private:

    std::string databaseURL;
    std::string outputName;
    cudaDeviceProp gpuDeviceProperties; // device properties for first GPU device found
    uint numInputVisibilities; // number of complex visibilities
    uint32_t numBaselines;
    uint32_t numChannels;
    int cudaInputGridSize; // cuda kernel grid size used for input of visibilities
    int cudaBlockSize; // cuda kernel block size
    #ifdef INPUT16BIT
        half2 *deviceData; // device array used on GPU to receive the input data from host
    #else
        cuFloatComplex *deviceData;
    #endif

};


#endif
