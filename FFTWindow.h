// Copyright 2020 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * FFTWindow.h
 * Andrew Ensor
 * C++ program for generating FFT windows for FP16/FP32 data
 *
*/

#ifndef FFTWINDOW_H
#define FFTWINDOW_H

#include <iostream>
#include <cuda_fp16.h>

#include "Definitions.h"

/**********************************************************************
 * 
 **********************************************************************/

class FFTWindow
{

public:

    enum class WindowType {NONE,BLACKMAN,DOLPH_CHEBYSHEV,FLATTOP_SR785, FLATTOP_HFT70, FLATTOP_HFT248D, HANNING, KAISER};

    /**********************************************************************
     * FFTWindow default constructor
     **********************************************************************/
    FFTWindow(WindowType windowType);

    /**********************************************************************
     * FFTWindow init that creates the array to hold window entries
     **********************************************************************/
    void init(uint windowWidth);

    /**********************************************************************
     * FFTWindow function that calculates and returns the window entries as a half array
     **********************************************************************/
    #ifdef INPUT16BIT
        half *getEntries();
    #else
        float *getEntries();
    #endif

    /**********************************************************************
     * FFTWindow cleanup that destroys the array
     **********************************************************************/
    void cleanup();

private:

    WindowType windowType;
    uint windowWidth;
    #ifdef INPUT16BIT
        half *entries;
    #else
        float *entries;
    #endif
    class Flattop;
    const double BESSEL_ACCURACY = 1E-12;
    const double BLACKMAN_ALPHA = 0.16;
    const double DOLPHCHEBYSHEV_ALPHA = 3.0;
    const double KAISER_ALPHA = 5.0;

    /**********************************************************************
     * FFTWindow getCoshInverse calculates the inverse of the cosh function
     **********************************************************************/
    double getCoshInverse(double x);

    /**********************************************************************
     * FFTWindow getChebyshevPolynomial calculates a Chebyshev polynomial
     **********************************************************************/
    double getChebyshevPolynomial(double x, int n);

    /**********************************************************************
     * FFTWindow getChebyshevPolynomial calculates an unnormalisedChebyshev polynomial
     **********************************************************************/
    double getDolphChebyshevWindowEntry(double i, int windowWidth, double tenPowerAlpha, double x0);

    /**********************************************************************
     * FFTWindow getZeroOrderModifiedBessel calculates an accurate Bessel function
    **********************************************************************/
    double getZeroOrderModifiedBessel(double x);


};


#endif
