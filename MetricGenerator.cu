// Copyright 2020 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyrightc
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * MetricGenerator.cu
 * Andrew Ensor
 * C++/CUDA program for generating metrics for complex FP16 visibilities received in memory blocks
 * Each memory block is assumed to store polarisations innermost together, then baselines consecutively for a single channel
 * Consecutive memory blocks are assumed to be for consecutive channels, wrapping around to repeat again from channel 0
 * Metric averaging is counted for each iteration across all the channels
 * Note this prototype is explicitly managing host<->device memory transfers rather than using cudaMallocManaged unified memory
 * Build with: nvcc Metric.cu MetricGenerator.cu -lrdmaapi -lrdmacm -libverbs -lcurl -lpthread -lm -arch=sm_60 -Xptxas -O3 -o metricgenerator
 * Without RDMA api library: nvcc Metric.cu MetricGenerator.cu ../rdma-data-transport/RDMAapi.c ../rdma-data-transport/RDMAmetricgenerator.c ../rdma-data-transport/RDMAmemorymanager.c ../rdma-data-transport/RDMAexchangeidcallbacks.c ../rdma-data-transport/RDMAlogger.c -lrdmacm -libverbs -lcurl -lpthread -lm  -arch=sm_60 -Xptxas -O3 -o metricgenerator
 * Run with: ./metricgenerator -b512 -t65536 -h32
 * Note -arch is specified so can use half2 arithmetic
 * Note -Xptxas -O3 are for compiled kernel code and host code optimizations
 * Note this project uses RDMA as a static library called rdmaapi.a
 */

#include "MetricGenerator.h"

/**********************************************************************
 * convertIntToFloatOnGPU Cuda kernel that converts specified number of
 * complex int values to float values in-place in the deviceData
 **********************************************************************/
#ifdef INPUT16BIT
    __global__ void convertIntToFloatOnGPU(half2 *deviceData, int n)
#else
    __global__ void convertIntToFloatOnGPU(cuFloatComplex *deviceData, int n)
#endif
{
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    if (i < n)
    {
        #ifdef INPUT16BIT
            short realComponent = __half_as_short(deviceData[i].x);
            short imagComponent = __half_as_short(deviceData[i].y);
            deviceData[i].x = __hdiv((half)realComponent, (half)(1<<15));
            deviceData[i].y = __hdiv((half)imagComponent, (half)(1<<15));
        #else
            int realComponent = __float_as_int(deviceData[i].x);
            int imagComponent = __float_as_int(deviceData[i].y);
            deviceData[i].x = ((float)realComponent) / (1<<31);
            deviceData[i].y = ((float)imagComponent) / (1<<31);
        #endif
    }
}


/**********************************************************************
 * Checks the Cuda status and logs any errors
 **********************************************************************/
cudaError_t MetricGenerator::checkCudaStatus()
{
    cudaError_t errorSynch = cudaGetLastError();
    cudaError_t errorAsync = cudaDeviceSynchronize(); // blocks host thread until all previously issued CUDA commands have completed
    if (errorSynch != cudaSuccess)
        logger(LOG_NOTICE, "Cuda synchronous error %s", cudaGetErrorString(errorSynch));
    if (errorAsync != cudaSuccess)
        logger(LOG_NOTICE, "Cuda asynchronous error %s", cudaGetErrorString(errorAsync));
    if (errorSynch != cudaSuccess)
        return errorSynch;
    else
        return errorAsync;
}


/**********************************************************************
 * MetricGenerator constructor
 **********************************************************************/
MetricGenerator::MetricGenerator(std::string databaseURL, std::string outputName)
{
    this->databaseURL = databaseURL;
    this->outputName = outputName;
}


/**********************************************************************
 * MetricGenerator init to set up working data buffers
 **********************************************************************/
void MetricGenerator::init(uint numInputVisibilities, uint32_t numBaselines, uint32_t numChannels)
{
    this->numInputVisibilities = numInputVisibilities; /* TODO: remove this field as may become unnecessary */
    this->numBaselines = numBaselines;
    this->numChannels = numChannels;

    // output GPU device properties
    int numGPUDevices = 0;
    cudaGetDeviceCount(&numGPUDevices);
    for (int i=0; i<numGPUDevices; i++)
    {
        cudaDeviceProp foundDeviceProperties;
        cudaGetDeviceProperties(&foundDeviceProperties, i);
        if (i == 0)
            this->gpuDeviceProperties = foundDeviceProperties;
        double peakMemoryBandwidth = 2.0 * foundDeviceProperties.memoryClockRate
            * foundDeviceProperties.memoryBusWidth/1.0E6;
        logger(LOG_NOTICE, "GPU device name %s, memory clock rate %dkHz,"
            " memory bus width %dbits, peak memory bandwidth %fGbps",
            foundDeviceProperties.name, foundDeviceProperties.memoryClockRate,
            foundDeviceProperties.memoryBusWidth, peakMemoryBandwidth);
    }

    // calculate suitable cuda blockSize and cuda gridSize
    // following https://developer.nvidia.com/blog/cuda-pro-tip-occupancy-api-simplifies-launch-configuration/
    int minGridSize;
    cudaOccupancyMaxPotentialBlockSize(&minGridSize, &this->cudaBlockSize, convertIntToFloatOnGPU, 0, 0);
    #ifdef DUALPOLARISED
        this->cudaInputGridSize = (this->numInputVisibilities*4+this->cudaBlockSize-1)/this->cudaBlockSize;   
    #else
        this->cudaInputGridSize = (this->numInputVisibilities+this->cudaBlockSize-1)/this->cudaBlockSize;   
    #endif
    logger(LOG_NOTICE, "Block size %d, minimum grid size %d, chosen grid size %d",
        this->cudaBlockSize, minGridSize, this->cudaInputGridSize);

    // allocate the device transfer buffer for input data block
    #ifdef INPUT16BIT
        size_t numBlockBytes = this->numInputVisibilities*sizeof(half2);
    #else
        size_t numBlockBytes = this->numInputVisibilities*sizeof(cuFloatComplex);
    #endif
    #ifdef DUALPOLARISED
        numBlockBytes *= 4; /* XX,XY,YX,YY visibility polarisation products */
    #else
        numBlockBytes *= 1; /* only one visiblity polarisation product */
    #endif
    cudaMalloc((void**)&deviceData, numBlockBytes);

    // initialise the libcurl library for the posting of data to InfluxDB
    curl_global_init(CURL_GLOBAL_ALL);
}


/**********************************************************************
 * MetricGenerator transferVisibilities to transfer input block and potentially convert to float values
 **********************************************************************/
void MetricGenerator::transferVisibilities(void* dataPointer)
{
    // transfer the input block to the device
    #ifdef INPUT16BIT
        half2 *inputBlock = (half2 *)dataPointer;
        size_t numBlockBytes = this->numInputVisibilities*sizeof(half2);
    #else
        cuFloatComplex *inputBlock = (cuFloatComplex *)dataPointer;
        size_t numBlockBytes = this->numInputVisibilities*sizeof(cuFloatComplex);
    #endif
    #ifdef DUALPOLARISED
        numBlockBytes *= 4; /* XX,XY,YX,YY visibility polarisation products */
    #else
        numBlockBytes *= 1; /* only one visiblity polarisation product */
    #endif
    cudaMemcpy(deviceData, inputBlock, numBlockBytes, cudaMemcpyHostToDevice);

    #ifdef INPUTINTEGER
        #ifdef DUALPOLARISED
            convertIntToFloatOnGPU<<<this->cudaInputGridSize, this->cudaBlockSize>>>(this->deviceData,
                this->numInputVisibilities*4);
        #else
            convertIntToFloatOnGPU<<<this->cudaInputGridSize, this->cudaBlockSize>>>(this->deviceData,
                this->numInputVisibilities);
        #endif
    #else
        // default is to accept FP16 or FP32 floating point values so no conversion required
    #endif
}


/**********************************************************************
 * MetricGenerator getNumInputVisibilities returns the number of complex data values per block
 **********************************************************************/
int MetricGenerator::getNumInputVisibilities()
{
    return this->numInputVisibilities;
}


/**********************************************************************
 * MetricGenerator getCudaBlockSize returns the CUDA block size
 **********************************************************************/
int MetricGenerator::getCudaBlockSize()
{
    return this->cudaBlockSize;
}


/**********************************************************************
 * MetricGenerator getDeviceData returns a pointer to the visibility data currently on device
 **********************************************************************/
#ifdef INPUT16BIT
    half2 *MetricGenerator::getDeviceData()
#else
    cuFloatComplex *MetricGenerator::getDeviceData()
#endif
{
    return this->deviceData;
}


/**********************************************************************
 * struct that holds a single POST request of metric data
 **********************************************************************/
struct httpPostParams
{
    pthread_t *postThread;
    std::string databaseURL;
    std::string outputName;
    std::string operandString;
    uint32_t (*channelFilter)[2];
    uint32_t numChannelIntervals;
    uint32_t *baselineFilter;
    uint32_t blFilterLength;
    std::string *polarisationFilter;
    uint32_t polFilterLength;
    float *postedData;
};


/**********************************************************************
 * httpPostThread performs the HTTP POST request in a separate pthread  
 **********************************************************************/
static void *httpPostThread(void *ptr)
{
    struct httpPostParams *params = (struct httpPostParams *)ptr;
    // set the post header information for InfluxDB
    std::string postRequestHeader = "http://" + params->databaseURL
        + ":8086/write?db=vismetrics&precision=ms";
    unsigned long long millisecondsSinceEpoch
        = std::chrono::system_clock::now().time_since_epoch() / std::chrono::milliseconds(1);
    // set the post body information for InfluxDB
    std::stringstream postRequestBody;
    bool isFirstMetric = true;
    for (uint chIndex=0; chIndex<params->numChannelIntervals; chIndex++)
    {
        uint32_t startChannel = params->channelFilter[chIndex][0];
        uint32_t rangeChannel = params->channelFilter[chIndex][1] - startChannel;
        for (uint blIndex=0; blIndex<params->blFilterLength; blIndex++)
        {
            uint32_t baseline = params->baselineFilter[blIndex];
            for (uint polIndex=0; polIndex<params->polFilterLength; polIndex++)
            {
                std::string polarisation = params->polarisationFilter[polIndex];
                uint32_t i = chIndex*params->blFilterLength*params->polFilterLength + blIndex*params->polFilterLength + polIndex;
                float fieldValue = params->postedData[i];
                if (isnan(fieldValue) || isinf(fieldValue))
                {
                    logger(LOG_NOTICE, "Non-finite metric value ignored for chst=%u, chrg=%u, bl=%u, pol=%s",
                        startChannel, rangeChannel, baseline, polarisation.c_str());
                }
                else
                { 
                    if (isFirstMetric)
                    {
                        isFirstMetric = false; /* do not append endl for first entry */
                    }
                    else
                    {
                        postRequestBody << std::endl;
                    }
                    postRequestBody << params->outputName << ",chst=" << std::setw(6) << std::setfill('0') << startChannel
                        << ",chrg=" << std::setw(6) << std::setfill('0') << rangeChannel;
                    postRequestBody << ",bl=" << std::setw(6) << std::setfill('0') << baseline;
                    postRequestBody << ",pol=" << polarisation;
                    postRequestBody << " " << params->operandString << "=" << fieldValue
                        << " " << millisecondsSinceEpoch;
                }
            }
        }
    }
    // send the post request using curl
    CURL *curl;
    CURLcode res;
    curl = curl_easy_init();
    if (curl != NULL)
    {
        curl_easy_setopt(curl, CURLOPT_URL, postRequestHeader.c_str()); // note libcurl expects char* rather than std::string
        const std::string postRequestBodyString(postRequestBody.str()); // note stringstream str returns only a temporary string
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postRequestBodyString.c_str());
        res = curl_easy_perform(curl);
        if (res != CURLE_OK)
            logger(LOG_WARNING, "Unable to post metrics via HTTP with curl error %s", curl_easy_strerror(res));
        // log the data posted to InfluxDB, note this needs to works fine even after spectrogram object destroyed
        logger(LOG_DEBUG, "HTTP Data Posted to InfluxDB: %s\n%s", postRequestHeader.c_str(), postRequestBodyString.c_str());
        curl_easy_cleanup(curl);
    }
    free(params->postedData); // corresponding to malloc in postDataToDatabase
    free(params->postThread); // corresponding to malloc in postDataToDatabase
    delete[] params->polarisationFilter; // corresponding to new in postDataToDatabase
    delete[] params->baselineFilter; // corresponding to new in postDataToDatabase
    delete[] params->channelFilter; // corresponding to new in postDataToDatabase
    delete params; // corresponding to new in postDataToDatabase
    return NULL;
}


/**********************************************************************
 * MetricGenerator postDataToDatabase posts the data block to a database via an HTTP request  
 **********************************************************************/
void MetricGenerator::postDataToDatabase(float *outputData, Metric metric)
{
    // copy the output data to a temporary memory block which will be available later for pthread
    // note don't use cudaHostAlloc for this as that would require cudaFreeHost in pthread (so cuda calls across multiple threads)
    uint numOutputBytes = metric.getNumOutputMetrics() * sizeof(float);
    float *postedData = NULL;
    postedData = (float*)malloc(numOutputBytes); // free is called in httpPostThread
    memcpy(postedData, outputData, numOutputBytes);

    // post the data to the time series database using a new pthread
    pthread_t *postThread = NULL;
    postThread = (pthread_t*)malloc(sizeof(pthread_t)); // free is called in httpPostThread
    pthread_attr_t pthreadAttributes;
    pthread_attr_init(&pthreadAttributes);
    pthread_attr_setstacksize(&pthreadAttributes, 65536); // 64k stack allocation for pthread
    pthread_attr_setdetachstate(&pthreadAttributes, PTHREAD_CREATE_DETACHED);

    httpPostParams *params = new httpPostParams; // delete is called in httpPostThread
    params->postThread = postThread;
    params->databaseURL = this->databaseURL;
    params->outputName = this->outputName;
    params->operandString = metric.getOperandString();
    params->numChannelIntervals = metric.getNumChannelIntervals();
    params->channelFilter = new uint32_t[params->numChannelIntervals][2]; // delete is called in httpPostThread
    for (uint chIndex=0; chIndex<params->numChannelIntervals; chIndex++)
    {
        for (uint isEnd=0; isEnd<2; isEnd++)
        {
            params->channelFilter[chIndex][isEnd] = metric.getChannelFilter(chIndex, isEnd);
        }
    }
    params->blFilterLength = metric.getBlFilterLength();
    params->baselineFilter = new uint32_t[params->blFilterLength]; // delete is called in httpPostThread
    for (uint blIndex=0; blIndex<params->blFilterLength; blIndex++)
    {
        params->baselineFilter[blIndex] = metric.getBaselineFilter(blIndex);
    }
    params->polFilterLength = metric.getPolFilterLength();
    params->polarisationFilter = new std::string[params->polFilterLength]; // delete is called in httpPostThread
    for (uint polIndex=0; polIndex<params->polFilterLength; polIndex++)
    {
        params->polarisationFilter[polIndex] = metric.getPolarisationFilter(polIndex);
    }
    params->postedData = postedData;
    if (pthread_create(postThread, &pthreadAttributes, &httpPostThread, (void*)params) != 0)
        logger(LOG_ERR, "Unable to create thread for posting metrics via HTTP");
}


/**********************************************************************
 * MetricGenerator cleanupBuffers to free the memory buffers
 **********************************************************************/
void MetricGenerator::cleanupBuffers()
{
    cudaDeviceSynchronize();
    int maxActiveBlocks;
    cudaOccupancyMaxActiveBlocksPerMultiprocessor(&maxActiveBlocks, convertIntToFloatOnGPU, this->cudaBlockSize, 0);
    cudaGetDeviceProperties(&this->gpuDeviceProperties, 0);
    float occupancy = (maxActiveBlocks*this->cudaBlockSize/this->gpuDeviceProperties.warpSize)
        / (float)(this->gpuDeviceProperties.maxThreadsPerMultiProcessor/this->gpuDeviceProperties.warpSize);
    logger(LOG_NOTICE, "Maximum active blocks %d, occupancy %f", maxActiveBlocks, occupancy);
    cudaFree(this->deviceData);
    // cleanup the libcurl library for the posting of data to InfluxDB
    curl_global_cleanup();
}


/**********************************************************************
 * parseCommandLineArguments parses the command line arguments for configuring the metric generator
 **********************************************************************/
void parseCommandLineArguments(int argc, char *argv[], enum logType &requestLevel,
    uint32_t &messageSize, uint32_t &numMemoryBlocks, uint32_t &numContiguousMessages, uint64_t &numTotalMessages,
    std::string &rdmaDeviceName, uint8_t &rdmaPort, int &gidIndex, std::string &identifierFileName,
    std::string &outputURL, uint32_t &numOutputAveraging, std::string &outputName,
    uint32_t &numBaselines, uint32_t &numChannels)
{
    int opt;
    while ((opt = getopt(argc, argv, "l:m:b:c:t:r:p:g:x:o:v:n:s:h:")) != -1)
    {
        switch (opt)
        {
            case 'l' :
                if (optarg != NULL)
                {
                    uint32_t level = strtoul(optarg, NULL, 10);
                    if (level > static_cast<int>(LOG_DEBUG))
                        requestLevel = LOG_DEBUG;
                    else
                        requestLevel = static_cast<logType>(level);
                    logger(LOG_DEBUG, "Command line argument: log level %d", requestLevel);
                }
                break;
            case 'm' :
                if (optarg != NULL)
                    messageSize = strtoul(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: messageSize %" PRIu32, messageSize);
                break;
            case 'b' :
                if (optarg != NULL)
                    numMemoryBlocks = strtoul(optarg, NULL, 10);
                if (numMemoryBlocks < 1)
                    numMemoryBlocks = 1;
                logger(LOG_DEBUG, "Command line argument: numMemoryBlocks %" PRIu32, numMemoryBlocks);
                break;
            case 'c' :
                if (optarg != NULL)
                    numContiguousMessages = strtoul(optarg, NULL, 10);
                if (numContiguousMessages < 1)
                    numContiguousMessages = 1;
                logger(LOG_DEBUG, "Command line argument: numContiguousMessages %" PRIu32, numContiguousMessages);
                break;
            case 't' :
                if (optarg != NULL)
                    numTotalMessages = strtoull(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: numTotalMessages %" PRIu64, numTotalMessages);
                break;
            case 'r' :
                if (optarg != NULL)
                    rdmaDeviceName = optarg;
                logger(LOG_DEBUG, "Command line argument: rdmaDevice %s", rdmaDeviceName.c_str());
                break;
            case 'p' :
                if (optarg != NULL)
                    rdmaPort = strtoul(optarg, NULL, 10);
                if (rdmaPort < 1)
                    rdmaPort = 1;
                else if (rdmaPort > 2)
                    rdmaPort = 2;
                logger(LOG_DEBUG, "Command line argument: rdmaPort %d", rdmaPort);
                break;
            case 'g' :
                if (optarg != NULL)
                    gidIndex = strtol(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: gidIndex %d", gidIndex);
                break;
            case 'x' :
                if (optarg != NULL)
                    identifierFileName = optarg;
                logger(LOG_DEBUG, "Command line argument: identifierFileName %s", identifierFileName.c_str());
                break;
            case 'o' :
                if (optarg != NULL)
                    outputURL = optarg;
                logger(LOG_DEBUG, "Command line argument: outputURL %s", outputURL.c_str());
                break;
            case 'v' :
                if (optarg != NULL)
                    numOutputAveraging = strtoul(optarg, NULL, 10);
                if (numOutputAveraging < 1)
                    numOutputAveraging = 1;
                logger(LOG_DEBUG, "Command line argument: numOutputAveraging %" PRIu32, numOutputAveraging);
                break;
            case 'n' :
                if (optarg != NULL)
                    outputName = optarg;
                logger(LOG_DEBUG, "Command line argument: outputName %s", outputName.c_str());
                break;
            case 's' :
                if (optarg != NULL)
                    numBaselines = strtoul(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: numBaselines %" PRIu32, numBaselines);
                break;
            case 'h' :
                if (optarg != NULL)
                    numChannels = strtoul(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: numChannels %" PRIu32, numChannels);
                break;
            default :
                  std::cout << "Invalid command line argument" << std::endl;
                logger(LOG_CRIT, "Usage: %s [-l log level 0..6]"
                    "[-m message size in bytes] [-b num memory blocks] [-c num contig messages per block] "
                    "[-t total num messages] "
                    "[-r rdma device name] [-p device port] [-g gid index] [-x exchange identifier filename] "
                    "[-o output URL] [-v output averaging] [-n output name] "
                    "[-s number visiblity baselines] [-h number visibility channels]",
                    argv[0]);
        }
    }
}


/**********************************************************************
 * Main method to execute
 **********************************************************************/
int main(int argc, char *argv[])
{
    std::cout << "Metric Generator starting";
    #ifdef INPUT16BIT
        #ifdef INPUTINTEGER
            std::cout << " for short precision complex input data";
        #else
            std::cout << " for half precision complex input data";
        #endif
    #else
        #ifdef INPUTINTEGER
            std::cout << " for int precision complex input data";
        #else
            std::cout << " for float precision complex input data";
        #endif
    #endif
    #ifdef DUALPOLARISED
        std::cout << " with dual polarisation products" << std::endl;
    #else
        std::cout << " with single polarisation product" << std::endl;
    #endif

    enum logType requestLogLevel = LOG_NOTICE;
    uint32_t messageSize = 65536; /* size in bytes of single RDMA message */
    uint32_t numMemoryBlocks = 1; /* number of memory blocks to allocate for RDMA messages */
    uint32_t numContiguousMessages = 1; /* number of contiguous messages to hold in each memory region */
    uint64_t numTotalMessages = 0; /* total number of messages to send or receive, if 0 then default to numMemoryBlocks*numContiguousMessages */
    std::string rdmaDeviceName; /* no preferred rdma device name to choose */
    uint8_t rdmaPort = 1;
    int gidIndex = -1; /* preferred gid index or -1 for no preference */
    std::string identifierFileName; /* default to using stdio for exchanging RDMA identifiers */
    std::string outputURL = "156.62.60.71"; /* default URL to which to post channelised power data */
    uint32_t numOutputAveraging = 1; /* number of input blocks in each channel to average in frequency domain for each output block */
    std::string outputName = "metrictest";
    uint32_t numBaselines = 0; /* number of baselines in visibility input data, if 0 then default to numInputVisibilities / number polarisation products */
    uint32_t numChannels = 0; /* number of channels in visibility input data, if 0 then default to numContiguousMessages */

    parseCommandLineArguments(argc, argv, requestLogLevel,
        messageSize, numMemoryBlocks, numContiguousMessages, numTotalMessages,
        rdmaDeviceName, rdmaPort, gidIndex, identifierFileName,
        outputURL, numOutputAveraging, outputName, numBaselines, numChannels);
    if (numTotalMessages == 0)
        numTotalMessages = numMemoryBlocks * numContiguousMessages;
    if (numChannels == 0)
        numChannels = numContiguousMessages;
    setLogLevel(requestLogLevel);

// TODO ALLOW MULTIPLE DATABLOCKS PER MESSAGE
    #ifdef DUALPOLARISED /* dual polarised input has XX,XY,YX,YY visibility polarisation products */
        #ifdef INPUT16BIT
            const uint numInputVisibilities = (messageSize/4)/sizeof(half2);
        #else
            const uint numInputVisibilities = (messageSize/4)/sizeof(cuFloatComplex);
        #endif
    #else /* single X polarised input has only XX visibility polarisation product */
        #ifdef INPUT16BIT
            const uint numInputVisibilities = messageSize/sizeof(half2);
        #else
            const uint numInputVisibilities = messageSize/sizeof(cuFloatComplex);
        #endif
    #endif
    if (numBaselines == 0)
        numBaselines = numInputVisibilities;

    MetricGenerator metricGenerator = MetricGenerator(outputURL, outputName);
    metricGenerator.init(numInputVisibilities, numBaselines, numChannels);

    Metric metric = Metric::Metric(numBaselines, numChannels, Metric::OperationType::OPERATION_MEAN, Metric::OperandType::OPERAND_AMP);

// TODO: make the following filters configurable
    /* set up some temporary test filters */
    #ifdef DUALPOLARISED
        uint32_t polarisationFilter[2] = {0,3};
    #else
        uint32_t polarisationFilter[1] = {0};
    #endif
    uint32_t baselineFilter[3] = {0,1,2};
    uint numChannelIntervals = min(128, numChannels); /* average channels if over threshold 100 */
    uint32_t channelFilter[numChannelIntervals][2];
    for (uint i=0; i<numChannelIntervals; i++)
    {
        channelFilter[i][0] = numChannels*i/numChannelIntervals;
        channelFilter[i][1] = numChannels*(i+1)/numChannelIntervals;
    }
    metric.init(polarisationFilter, sizeof(polarisationFilter)/sizeof(polarisationFilter[0]),
        baselineFilter, sizeof(baselineFilter)/sizeof(baselineFilter[0]),
        channelFilter, sizeof(channelFilter)/sizeof(channelFilter[0]),
        metricGenerator.getCudaBlockSize());

    /* allocate page-locked device-accessible memory blocks as buffers to be used in the memory regions */
    #ifdef INPUT16BIT
        size_t numInputBytes = metricGenerator.getNumInputVisibilities()*sizeof(half2);
        half2 *inputData[numMemoryBlocks];
    #else
        size_t numInputBytes = metricGenerator.getNumInputVisibilities()*sizeof(cuFloatComplex);
        cuFloatComplex *inputData[numMemoryBlocks];
    #endif
    #ifdef DUALPOLARISED
        numInputBytes *= 4;
    #else
        numInputBytes *= 1;
    #endif
    for (unsigned int blockIndex=0; blockIndex<numMemoryBlocks; blockIndex++)
    {
        cudaHostAlloc(&inputData[blockIndex], numInputBytes*numContiguousMessages, 0); // Note including cudaHostAllocWriteCombined flag fails to register for RDMA
    }

    /* create a memory region manager to manage the usage of the memory blocks */
    /* note for memory region manager is monitoring memory regions so can get LOG_WARNING when metric generator not keeping up with RDMA receiver */
    MemoryRegionManager* manager = createMemoryRegionManager((void **)inputData,
        messageSize, numMemoryBlocks, numContiguousMessages, numTotalMessages, true);
    setAllMemoryRegionsPopulated(manager, false);

    // allocate the output buffer which will hold the results
    float *outputData;
    size_t numOutputBytes = metric.getNumOutputMetrics()*sizeof(float);
    cudaHostAlloc(&outputData, numOutputBytes, 0);
    metricGenerator.checkCudaStatus();

    /* set up timers */
    cudaEvent_t startTime, stopTime;
    cudaEventCreate(&startTime);
    cudaEventCreate(&stopTime);
    float executionTime = 0; // milliseconds
    cudaEventRecord(startTime);

    char *rdmaDeviceNameChars = NULL; /* no preferred rdma device name to choose */
    if (!rdmaDeviceName.empty())
        rdmaDeviceNameChars = const_cast<char*>(rdmaDeviceName.c_str());
    char *identifierFileNameChars = NULL; /* default to using stdio for exchanging RDMA identifiers */
    if (!identifierFileName.empty())
        identifierFileNameChars = const_cast<char*>(identifierFileName.c_str());
    char *metricURL = NULL; /* default to not push metrics */
    uint32_t numMetricAveraging = 0; /* number of message completions over which to average metrics, default to numMemoryBlocks*numContiguousMessages */

    /* create a pointer to an identifier exchange function (see eg RDMAexchangeidentifiers for some examples) */
    enum exchangeResult (*identifierExchangeFunction)(bool isSendMode,
        uint32_t packetSequenceNumber, uint32_t queuePairNumber, union ibv_gid gidAddress, uint16_t localIdentifier,
        uint32_t *remotePSNPtr, uint32_t *remoteQPNPtr, union ibv_gid *remoteGIDPtr, uint16_t *remoteLIDPtr) = NULL;
    if (identifierFileNameChars != NULL)
    {
        setIdentifierFileName(identifierFileNameChars);
        identifierExchangeFunction = exchangeViaSharedFiles;
    }

    /* perform the RDMA transfers */
    rdmaTransferMemoryBlocks(RECV_MODE, manager,
        0, identifierExchangeFunction,
        rdmaDeviceNameChars, rdmaPort, gidIndex,
        metricURL, numMetricAveraging);

    /* process the message data as they get transferred */
    uint64_t expectedTransfer = 0;
    uint32_t currentRegionIndex = 0;
    uint32_t currentContiguousIndex = 0;
    uint32_t currentChannel = 0; /* each RDMA message is presumed to be one channel of visibilities sent channel-ordered */
    uint numAccumulations = 0; /* current number of times all channels have been accumulated */
    while (expectedTransfer < numTotalMessages)
    {
        /* delay until the current memory location has its data transferred */
        uint64_t currentTransfer = getMessageTransferred(manager, currentRegionIndex, currentContiguousIndex);
        while (currentTransfer == NO_MESSAGE_ORDINAL || currentTransfer < expectedTransfer)
        {
            /* expected message transfer has not yet taken place */
            microSleep(10); /* sleep current thread before checking currentTransfer again */
            currentTransfer = getMessageTransferred(manager, currentRegionIndex, currentContiguousIndex);
        }
        /* process the current message transfer */
        if (currentTransfer > expectedTransfer)
        {
            /* messages between expectedTransfer and currentTransfer-1 inclusive have been dropped during the transfer */
            /* HERE: do something about the missing messages */
            logger(LOG_WARNING, "Dropped messages detected from channel %u with %lu messages dropped",
                currentChannel, currentTransfer-expectedTransfer);  
            currentChannel += currentTransfer-expectedTransfer; /* skip missing channels */
            if (currentChannel >= numChannels)
            {
                currentChannel = currentChannel % numChannels; /* wrap around channels */
                numAccumulations++;
            }
        }
        /* determine whether currentChannel falls within the channel filter at channelIntervalIndex */
        if (metric.isAccumulatingChannel(currentChannel))
        {
            #ifdef DUALPOLARISED
                unsigned int inputStartIndex = 4*currentContiguousIndex*metricGenerator.getNumInputVisibilities();
            #else
                unsigned int inputStartIndex = currentContiguousIndex*metricGenerator.getNumInputVisibilities();
            #endif
            metricGenerator.transferVisibilities(&inputData[currentRegionIndex][inputStartIndex]);
            metric.accumulateMetric(metricGenerator.getDeviceData());
        }
        /* once done processing the current message transfer move along to the next memory location */
        expectedTransfer = currentTransfer + 1;
        currentContiguousIndex++;
        if (currentContiguousIndex >= manager->numContiguousMessages)
        {
            /* move along to the start of the next memory region */
            setMemoryRegionPopulated(manager, currentRegionIndex, false); /* finished processing this memory region so it can now be reused */
            currentContiguousIndex = 0;
            currentRegionIndex++;
            if (currentRegionIndex >= manager->numMemoryRegions)
            {
                /* wrap around to reuse the same memory regions */
                currentRegionIndex = 0;
            }
        }
        /* move along to the next channel for the next incoming RDMA message */
        currentChannel++;
        if (currentChannel >= numChannels)
        {
            currentChannel = 0;
            numAccumulations++;
        }
        if (numAccumulations >= numOutputAveraging)
        {
            logger(LOG_DEBUG, "Performing resolution of datablock accumulations");
            metric.resolveAndReturn(outputData);

            cudaEventRecord(stopTime);
            cudaEventSynchronize(stopTime);
            cudaEventElapsedTime(&executionTime, startTime, stopTime);
            logger(LOG_INFO, "Average execution time is %.2fms", executionTime);
            metricGenerator.postDataToDatabase(outputData, metric);
            numAccumulations = 0;
            cudaEventRecord(startTime);
        }
    }

    /* ensure the RDMA transfers taking place in a separate thread have completed by making current thread wait until they have completed */
    waitForRdmaTransferCompletion(manager); 

    /* destroy timers */
    cudaEventDestroy(startTime);
    cudaEventDestroy(stopTime);

    /* free memory block buffers */
    cudaFreeHost(outputData);
    destroyMemoryRegionManager(manager);
    for (unsigned int blockIndex=0; blockIndex<numMemoryBlocks; blockIndex++)
    {
        cudaFreeHost(inputData[blockIndex]);
    }
    metric.cleanup();
    metricGenerator.cleanupBuffers();

    metricGenerator.checkCudaStatus();

    std::cout << "Metric Generator ending" << std::endl;
    return 0;
}