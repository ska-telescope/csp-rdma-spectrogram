// Copyright 2020 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * FFTWindow.cu
 * Andrew Ensor
 * C++ program for generating FFT windows for FP16/FP32 data
 *
*/

#include "FFTWindow.h"

class FFTWindow::Flattop
{
private:

    const double *coefficients;
    uint coefficientsLength;

public:

    void init(const double *coefficients, uint coefficientsLength)
    {
        this->coefficients = coefficients;
        this->coefficientsLength = coefficientsLength;
    }

    #ifdef INPUT16BIT
        void calcEntries(half *entries, uint windowWidth)
    #else
        void calcEntries(float *entries, uint windowWidth)
    #endif
    {
        double term = 2 * M_PI / (windowWidth - 1);
        double midPoint = (windowWidth - 1) / 2.0;
        double midPointSum = coefficients[0];
        for (uint freq=1; freq<coefficientsLength; freq++)
        {
            midPointSum += coefficients[freq] * std::cos(term * freq * midPoint);
        }
        for (uint i=0; i<windowWidth; i++)
        {
            double sum = coefficients[0];
            for (uint freq=1; freq<coefficientsLength; freq++)
            {
                sum += coefficients[freq] * std::cos(term * freq * i);
            }
            #ifdef INPUT16BIT
                entries[i] = (half)(sum/midPointSum);
            #else
                entries[i] = (float)(sum/midPointSum);
            #endif
        }
    }   
};


/**********************************************************************
 * FFTWindow default constructor
 **********************************************************************/
FFTWindow::FFTWindow(WindowType windowType)
{
    this->windowType = windowType;
}


/**********************************************************************
 * FFTWindow init that creates the array to hold window entries
 **********************************************************************/
void FFTWindow::init(uint windowWidth)
{
    this->windowWidth = windowWidth;
    #ifdef INPUT16BIT
        cudaHostAlloc(&entries, windowWidth*sizeof(half), 0);
    #else
        cudaHostAlloc(&entries, windowWidth*sizeof(float), 0);
    #endif
}


/**********************************************************************
 * FFTWindow getCoshInverse calculates the inverse of the cosh function
 **********************************************************************/
double FFTWindow::getCoshInverse(double x)
{
    return std::log(x + std::sqrt(x * x - 1));
}


/**********************************************************************
 * FFTWindow getChebyshevPolynomial calculates a Chebyshev polynomial
 **********************************************************************/
double FFTWindow::getChebyshevPolynomial(double x, int n)
{
    if (x == 0)
    {
        if (n % 4 == 0)
            return 1.0;
        else if (n % 4 == 2)
            return -1.0;
        else
            return 0.0;
    }
    else if (x == 1)
    {
        return 1.0;
    }
    else if (std::abs(x) < 1.0)
    {
        return cos(n * std::acos(x));
    }
    else
    {
        return cosh(n * getCoshInverse(x));
    }
}


/**********************************************************************
 * FFTWindow getChebyshevPolynomial calculates an unnormalised Chebyshev polynomial
 **********************************************************************/
double FFTWindow::getDolphChebyshevWindowEntry(double i, int windowWidth,
    double tenPowerAlpha, double x0)
{
    double midPoint = (windowWidth - 1) / 2.0;
    // check whether fields need to be recalculated
    if (i > midPoint)
    {
        i = windowWidth - 1 - i;
    }
    double sum = 0;
    // accumulate terms from smaller to larger
    for (int j = (int)midPoint; j >= 1; j--)
    {
        double x = x0 * std::cos(M_PI * j / windowWidth);
        double term = getChebyshevPolynomial(x, windowWidth - 1)
            * std::cos(2 * M_PI * (i - midPoint) * j / windowWidth);
        sum += term;
    }
    sum *= 2;
    sum = (sum + tenPowerAlpha) / windowWidth;
    return sum;
}


/**********************************************************************
 * FFTWindow getZeroOrderModifiedBessel calculates an accurate Bessel function
 **********************************************************************/
double FFTWindow::getZeroOrderModifiedBessel(double x)
{
    double sum = 0.0;
    double term = 1.0;
    int m = 0;
    do
    {
        // calculate term carefully to avoid overflows
        term = 1.0;
        for (int i = 1; i <= m; i++)
        {
            term *= x / (2.0 * i);
        }
        term *= term; // square term
        sum += term;
        m++;
    }
    while (std::abs(term / sum) > BESSEL_ACCURACY);
    return sum;
}


/**********************************************************************
 * FFTWindow getEntries that calculates and returns the window entries as a half array
 **********************************************************************/
#ifdef INPUT16BIT
    half *FFTWindow::getEntries()
#else
    float *FFTWindow::getEntries()
#endif
{
    if (this->windowType==WindowType::FLATTOP_SR785
        || this->windowType==WindowType::FLATTOP_HFT70
        || this->windowType==WindowType::FLATTOP_HFT248D)
    {
        Flattop flattop;
        switch(this->windowType)
        {
            case WindowType::FLATTOP_SR785 :
            {
                const double coefficients[] = {1.0, -1.93, 1.29, -0.388, 0.028};
                const int coefficientsLength = 5;
                flattop.init(coefficients, coefficientsLength);
                flattop.calcEntries(entries, windowWidth);
                break;
            }
            case WindowType::FLATTOP_HFT70 :
            {
                const double coefficients[] = {1.0, -1.90796, 1.07349, -0.18199};
                const int coefficientsLength = 4;
                flattop.init(coefficients, coefficientsLength);
                flattop.calcEntries(entries, windowWidth);
                break;
            }
            case WindowType::FLATTOP_HFT248D :
            {
                const double coefficients[] = {1.0, -1.985844164102, 1.791176438506,
                    -1.282075284005, 0.667777530266, -0.240160796576, 0.056656381764,
                    -0.008134974479, 0.000624544650, -0.000019808998, 0.000000132974};
                const int coefficientsLength = 11;
                flattop.init(coefficients, coefficientsLength);
                flattop.calcEntries(entries, windowWidth);
                break;
            }
        }
        return entries;
    }
    else
    {
        switch(this->windowType)
        {
            case WindowType::BLACKMAN :
            {
                for (uint i=0; i<windowWidth; i++)
                {
                    double entry = (1 - BLACKMAN_ALPHA) / 2 - 0.5 * std::cos(2 * M_PI * i / (windowWidth - 1))
                        + BLACKMAN_ALPHA / 2 * std::cos(4 * M_PI * i / (windowWidth - 1));
                    #ifdef INPUT16BIT
                        entries[i] = (half)entry;
                    #else
                        entries[i] = (float)entry;
                    #endif
                }
                break;
            }
            case WindowType::DOLPH_CHEBYSHEV :
            {
                double tenPowerAlpha = std::pow(10, DOLPHCHEBYSHEV_ALPHA);
                double x0 = std::cosh(getCoshInverse(tenPowerAlpha) / (windowWidth - 1));
                double midPoint = (windowWidth - 1) / 2.0;
                // calculate maximum value which is at the midPoint
                double maxValue = 1.0;
                maxValue = getDolphChebyshevWindowEntry(midPoint, windowWidth, tenPowerAlpha, x0);        
                for (uint i=0; i<windowWidth; i++)
                {
                    // calculate a normalised Dolph-Chebyshev window entry
                    double entry = getDolphChebyshevWindowEntry(i, windowWidth, tenPowerAlpha, x0) / maxValue;
                    #ifdef INPUT16BIT
                        entries[i] = (half)entry;
                    #else
                        entries[i] = (float)entry;
                    #endif
                }
                break;
            }
            case WindowType::HANNING :
            {
                for (uint i=0; i<windowWidth; i++)
                {
                    double entry = 0.5434783 - 0.4565217 * cos(2 * M_PI * i / (windowWidth - 1));
                    #ifdef INPUT16BIT
                        entries[i] = (half)entry;
                    #else
                        entries[i] = (float)entry;
                    #endif
                }
                break;
            }
            case WindowType::KAISER :
            {
                double besselPiAlpha = getZeroOrderModifiedBessel(M_PI*KAISER_ALPHA);
                for (uint i=0; i<windowWidth; i++)
                {
                    double innerTerm = 2.0 * i / (windowWidth - 1) - 1;
                    double x = M_PI * KAISER_ALPHA * std::sqrt(1.0 - innerTerm * innerTerm);
                    double entry = getZeroOrderModifiedBessel(x) / besselPiAlpha;
                    #ifdef INPUT16BIT
                        entries[i] = (half)entry;
                    #else
                        entries[i] = (float)entry;
                    #endif
                }
                break;
            }
            case WindowType::NONE :
            {
                for (uint i=0; i<windowWidth; i++)
                {
                    #ifdef INPUT16BIT
                        entries[i] = (half)1.0f;
                    #else
                        entries[i] = 1.0f;
                    #endif
                }
                break;
            }
            default :
                std::cout << "Window not yet implemented " << std::endl;
                break;
        }
    }
    return entries; 
}


/**********************************************************************
 * FFTWindow cleanup that destroys the array
 **********************************************************************/
void FFTWindow::cleanup()
{
    cudaFreeHost(entries);
}
