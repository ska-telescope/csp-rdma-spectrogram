// Copyright 2020 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * Spectrogram.h
 * Andrew Ensor
 * C++/CUDA program for generating spectrogram for complex FP16 data received in memory blocks
 *
*/

#ifndef SPECTROGRAM_H
#define SPECTROGRAM_H

#include <chrono>
#include <iomanip>
#include <iostream>
#include <string>
#include <sstream>
#include <cuda_fp16.h>
#include <cufft.h>
#include <cufftXt.h>
#include <curl/curl.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h> // used for getopt and optarg in parseCommandLineArguments

#include "Definitions.h"
#include "FFTWindow.h"

// import the C-based RDMA api
#ifdef __cplusplus
    extern "C"
    {
#endif
#include <RDMAapi.h>
#ifdef __cplusplus
    }
#endif


/**********************************************************************
 * 
 **********************************************************************/

class Spectrogram
{

public:

    /**********************************************************************
     * Returns the Cuda status and logs any synch or asynch errors
     **********************************************************************/
    cudaError_t checkCudaStatus();

    /**********************************************************************
     * Spectrogram default constructor
     **********************************************************************/
    Spectrogram(std::string databaseURL, std::string outputName);

    /**********************************************************************
     * Spectrogram init to set up working data buffers and cuFFT plan
     **********************************************************************/
    void init(uint numInputSamples, FFTWindow *window);

    /**********************************************************************
     * Spectrogram accumulateFFT to transfer input block, perform FFT and accumulate
     **********************************************************************/
    void accumulateFFT(void *dataPointer);

    /**********************************************************************
     * Spectrogram averageAndReturn to average results in deviceData and return to host buffer
     **********************************************************************/
    void averageAndReturn(float *outputData, uint numAverage);

    /**********************************************************************
     * Spectrogram getNumInputSamples returns the number of complex data values per block
     **********************************************************************/
    inline int getNumInputSamples();

    /**********************************************************************
     * Spectrogram postDataToDatabase posts the data block to a database via an HTTP request  
     **********************************************************************/
    void postDataToDatabase(float *outputData, uint startChannel, uint endChannel);

    /**********************************************************************
     * Spectrogram cleanupBuffers to free the memory buffers
     **********************************************************************/
    void cleanupBuffers();

private:

    std::string databaseURL;
    std::string outputName;
    FFTWindow *window;
    cudaDeviceProp gpuDeviceProperties; // device properties for first GPU device found
    uint numInputSamples; // number of complex data values in each data input that will be used in FFT and averaged
    int cudaGridSize; // cuda kernel grid size
    int cudaBlockSize; // cuda kernel block size
    #ifdef INPUT16BIT
        half2 *deviceData; // device array used on GPU to receive the input data from host
        half *deviceWindow; // device array used on GPU to receive the window data from host
    #else
        cufftComplex *deviceData;
        float *deviceWindow;
    #endif
    float *deviceSum; // device data block used on GPU to store the accumulated power summations
    cufftHandle plan;

};


#endif
