// Copyright 2020 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * SimulatedVisibilityGenerator.cpp
 * Andrew Ensor
 * C++ program for generating an input signal to test MetricGenerator.cu
 * Build with: g++ SimulatedVisibilityGenerator.cpp -lrdmaapi -lrdmacm -libverbs -lcurl -lpthread -lm -O3 -o simulatedvisibilitygenerator
 * Run with: ./simulatedvisibilitygenerator -b32 -t65536 -d10
 * Note -O3 is for host code optimizations
 * Note this project uses RDMA as a static library called rdmaapi.a
 * Note instead of CUDA's cuda_fp16 this project uses the half-2.1.0 C++ floating-point library for FP16 support available from http://half.sourceforge.net/
*/

#include <iostream>
#include <math.h>
#include <string>
#include <unistd.h> // used for getopt and optarg in parseCommandLineArguments

#include "../half-2.1.0/include/half.hpp"

#include "Definitions.h"

// import the C-based RDMA api
#ifdef __cplusplus
    extern "C"
    {
#endif
#include <RDMAapi.h>
#ifdef __cplusplus
    }
#endif

using half_float::half;
struct half2
{
    half x;
    half y;
};
struct float2
{
    float x;
    float y;
};

/**********************************************************************
 * parseCommandLineArguments parses the command line arguments for configuring the spectrogram
 **********************************************************************/
void parseCommandLineArguments(int argc, char *argv[], enum logType &requestLevel,
    uint32_t &messageSize, uint32_t &numMemoryBlocks, uint32_t &numContiguousMessages, uint64_t &numTotalMessages,
    uint32_t &messageDelayTime, std::string &rdmaDeviceName, uint8_t &rdmaPort, int &gidIndex, std::string &identifierFileName,
    uint32_t &numBaselines, uint32_t &numChannels)
{
    int opt;
    while ((opt = getopt(argc, argv, "l:m:b:c:t:d:r:p:g:x:s:h:")) != -1)
    {
        switch (opt)
        {
            case 'l' :
                if (optarg != NULL)
                {
                    uint32_t level = strtoul(optarg, NULL, 10);
                    if (level > static_cast<int>(LOG_DEBUG))
                        requestLevel = LOG_DEBUG;
                    else
                        requestLevel = static_cast<logType>(level);
                    logger(LOG_DEBUG, "Command line argument: log level %d", requestLevel);
                }
                break;
            case 'm' :
                if (optarg != NULL)
                    messageSize = strtoul(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: messageSize %" PRIu32, messageSize);
                break;
            case 'b' :
                if (optarg != NULL)
                    numMemoryBlocks = strtoul(optarg, NULL, 10);
                if (numMemoryBlocks < 1)
                    numMemoryBlocks = 1;
                logger(LOG_DEBUG, "Command line argument: numMemoryBlocks %" PRIu32, numMemoryBlocks);
                break;
            case 'c' :
                if (optarg != NULL)
                    numContiguousMessages = strtoul(optarg, NULL, 10);
                if (numContiguousMessages < 1)
                    numContiguousMessages = 1;
                logger(LOG_DEBUG, "Command line argument: numContiguousMessages %" PRIu32, numContiguousMessages);
                break;
            case 't' :
                if (optarg != NULL)
                    numTotalMessages = strtoull(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: numTotalMessages %" PRIu64, numTotalMessages);
                break;
            case 'd' :
                if (optarg != NULL)
                    messageDelayTime = strtoul(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: messageDelayTime %" PRIu32, messageDelayTime);
                break;
            case 'r' :
                if (optarg != NULL)
                    rdmaDeviceName = optarg;
                logger(LOG_DEBUG, "Command line argument: rdmaDevice %s", rdmaDeviceName);
                break;
            case 'p' :
                if (optarg != NULL)
                    rdmaPort = strtoul(optarg, NULL, 10);
                if (rdmaPort < 1)
                    rdmaPort = 1;
                else if (rdmaPort > 2)
                    rdmaPort = 2;
                logger(LOG_DEBUG, "Command line argument: rdmaPort %d", rdmaPort);
                break;
            case 'g' :
                if (optarg != NULL)
                    gidIndex = strtol(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: gidIndex %d", gidIndex);
                break;
            case 'x' :
                if (optarg != NULL)
                    identifierFileName = optarg;
                logger(LOG_DEBUG, "Command line argument: identifierFileName %s", identifierFileName);
                break;
            case 's' :
                if (optarg != NULL)
                    numBaselines = strtoul(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: numBaselines %" PRIu32, numBaselines);
                break;
            case 'h' :
                if (optarg != NULL)
                    numChannels = strtoul(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: numChannels %" PRIu32, numChannels);
                break;
            default :
                  std::cout << "Invalid command line argument" << std::endl;
                logger(LOG_CRIT, "Usage: %s [-l log level 0..6]"
                    "[-m message size in bytes] [-b num memory blocks] [-c num contig messages per block] "
                    "[-t total num messages] [-d delay microsec time per message] "
                    "[-r rdma device name] [-p device port] [-g gid index] [-x exchange identifier filename] ",
                    "[-s number visiblity baselines] [-h number visibility channels]",
                    argv[0]);
        }
    }
}


/**********************************************************************
 * Main method to execute
 **********************************************************************/
int main(int argc, char *argv[])
{
    std::cout << "Simulated Visibility Generator starting";
    #ifdef INPUT16BIT
        #ifdef INPUTINTEGER
            std::cout << " for short precision complex input data";
        #else
            std::cout << " for half precision complex input data";
        #endif
    #else
        #ifdef INPUTINTEGER
            std::cout << " for int precision complex input data";
        #else
            std::cout << " for float precision complex input data";
        #endif
    #endif
    #ifdef DUALPOLARISED
        std::cout << " with dual polarisations" << std::endl;
    #else
        std::cout << " with single polarisation" << std::endl;
    #endif

    enum logType requestLogLevel = LOG_INFO;
    uint32_t messageSize = 65536; /* size in bytes of single RDMA message */
    uint32_t numMemoryBlocks = 1; /* number of memory blocks to allocate for RDMA messages */
    uint32_t numContiguousMessages = 1; /* number of contiguous messages to hold in each memory region */
    uint64_t numTotalMessages = 0; /* total number of messages to send or receive, if 0 then default to numMemoryBlocks*numContiguousMessages */
    uint32_t messageDelayTime = 0; /* time in milliseconds to delay after each message send/receive posted, default is no delay */
    std::string rdmaDeviceName; /* no preferred rdma device name to choose */
    uint8_t rdmaPort = 1;
    int gidIndex = -1; /* preferred gid index or -1 for no preference */
    std::string identifierFileName; /* default to using stdio for exchanging RDMA identifiers */
    uint32_t numBaselines = 0; /* number of baselines in visibility input data, if 0 then default to numInputVisibilities / number polarisation products */
    uint32_t numChannels = 0; /* number of channels in visibility input data, if 0 then default to numContiguousMessages */

    parseCommandLineArguments(argc, argv, requestLogLevel,
        messageSize, numMemoryBlocks, numContiguousMessages, numTotalMessages, messageDelayTime,
        rdmaDeviceName, rdmaPort, gidIndex, identifierFileName, numBaselines, numChannels);
    if (numTotalMessages == 0)
        numTotalMessages = numMemoryBlocks * numContiguousMessages;
    if (numChannels == 0)
        numChannels = numContiguousMessages;
    setLogLevel(requestLogLevel);

// TODO ALLOW MULTIPLE DATABLOCKS PER MESSAGE
    #ifdef DUALPOLARISED /* dual polarised input has XX,XY,YX,YY visibility polarisation products */
        #ifdef INPUT16BIT
            const uint numInputVisibilities = (messageSize/4)/sizeof(half2);
        #else
            const uint numInputVisibilities = (messageSize/4)/sizeof(cuFloatComplex);
        #endif
        const uint numInputSamples = numInputVisibilities * 4;
    #else /* single X polarised input has only XX visibility polarisation product */
        #ifdef INPUT16BIT
            const uint numInputVisibilities = messageSize/sizeof(half2);
        #else
            const uint numInputVisibilities = messageSize/sizeof(cuFloatComplex);
        #endif
        const uint numInputSamples = numInputVisibilities * 1;
    #endif
    if (numBaselines == 0)
        numBaselines = numInputVisibilities;

    // allocate the input buffer
    #ifdef INPUT16BIT
        size_t numInputBytes = numInputSamples*sizeof(half2);
        half2 *inputData[numMemoryBlocks];
    #else
        size_t numInputBytes = numInputSamples*sizeof(float2);
        float2 *inputData[numMemoryBlocks];
    #endif
    for (unsigned int blockIndex=0; blockIndex<numMemoryBlocks; blockIndex++)
    {
        #ifdef INPUT16BIT
            inputData[blockIndex] = (half2 *)malloc(numInputBytes*numContiguousMessages);
        #else
            inputData[blockIndex] = (float2 *)malloc(numInputBytes*numContiguousMessages);
        #endif
    }

    /* create a memory region manager to manage the usage of the memory blocks */
    /* note for simplicity memory region manager doesn't monitor memory regions so this code doesn't have to track sending/receiving */
    MemoryRegionManager* manager = createMemoryRegionManager((void **)inputData,
        messageSize, numMemoryBlocks, numContiguousMessages, numTotalMessages, false);
    setAllMemoryRegionsPopulated(manager, false);

    /* populate some test data for testing MetricGenerator.cu */
    srand(time(NULL)); // initialise random seed
    logger(LOG_INFO, "Metric Generator test data generation in progress");
    for (unsigned int blockIndex=0; blockIndex<numMemoryBlocks; blockIndex++)
    {
        for (unsigned int contiguousIndex=0; contiguousIndex<numContiguousMessages; contiguousIndex++)
        {
            unsigned int sample = blockIndex*numContiguousMessages+contiguousIndex;
            // populate the input buffers with some test data
            // note these data are not realistic visibilities, only used to test mean, min, max, variance metrics
            for (unsigned int i=0; i<numInputVisibilities; i++)
            {
                float realXXPol
                    = 0.05*std::cos(i*2.0*M_PI*1.0/numInputVisibilities);
                float imagXXPol
                    = 0.05*std::sin(i*2.0*M_PI*1.0/numInputVisibilities);
                float realYYPol
                    = 0.01*std::cos(i*2.0*M_PI*3.0/numInputVisibilities) + 0.02*blockIndex/numMemoryBlocks;
                float imagYYPol
                    = 0.01*std::sin(i*2.0*M_PI*3.0/numInputVisibilities) - 0.01*blockIndex/numMemoryBlocks;
                #ifdef DUALPOLARISED
                    unsigned int inputIndex = 4*contiguousIndex*numInputVisibilities + 4*i;
                    #ifdef INPUT16BIT
                        #ifdef INPUTINTEGER
                            short realXXPolAsShort = (short)(realXXPol*(1<<15));
                            short imagXXPolAsShort = (short)(imagXXPol*(1<<15));
                            short realYYPolAsShort = (short)(realYYPol*(1<<15));
                            short imagYYPolAsShort = (short)(imagYYPol*(1<<15));
                            inputData[blockIndex][inputIndex].x = *reinterpret_cast<half *>(&realXXPolAsShort);
                            inputData[blockIndex][inputIndex].y = *reinterpret_cast<half *>(&imagXXPolAsShort);
                            inputData[blockIndex][inputIndex+1].x = 0;
                            inputData[blockIndex][inputIndex+1].y = 0;
                            inputData[blockIndex][inputIndex+2].x = 0;
                            inputData[blockIndex][inputIndex+2].y = 0;
                            inputData[blockIndex][inputIndex+3].x = *reinterpret_cast<half *>(&realYYPolAsShort);
                            inputData[blockIndex][inputIndex+3].y = *reinterpret_cast<half *>(&imagYYPolAsShort);
                        #else
                            inputData[blockIndex][inputIndex].x = (half)realXXPol;
                            inputData[blockIndex][inputIndex].y = (half)imagXXPol;
                            inputData[blockIndex][inputIndex+1].x = 0;
                            inputData[blockIndex][inputIndex+1].y = 0;
                            inputData[blockIndex][inputIndex+2].x = 0;
                            inputData[blockIndex][inputIndex+2].y = 0;
                            inputData[blockIndex][inputIndex+3].x = (half)realYYPol;
                            inputData[blockIndex][inputIndex+3].y = (half)imagYYPol;
                        #endif
                    #else
                        #ifdef INPUTINTEGER
                            int realXXPolAsInt = (int)(realXXPol*(1<<31));
                            int imagXXPolAsInt = (int)(imagXXPol*(1<<31));
                            int realYYPolAsInt = (int)(realYYPol*(1<<31));
                            int imagYYPolAsInt = (int)(imagYYPol*(1<<31));
                            inputData[blockIndex][inputIndex].x = *reinterpret_cast<float *>(&realXXPolAsInt);
                            inputData[blockIndex][inputIndex].y = *reinterpret_cast<float *>(&imagXXPolAsInt);
                            inputData[blockIndex][inputIndex+1].x = 0;
                            inputData[blockIndex][inputIndex+1].y = 0;
                            inputData[blockIndex][inputIndex+2].x = 0;
                            inputData[blockIndex][inputIndex+2].y = 0;
                            inputData[blockIndex][inputIndex+3].x = *reinterpret_cast<float *>(&realYYPolAsInt);
                            inputData[blockIndex][inputIndex+3].y = *reinterpret_cast<float *>(&imagYYPolAsInt);
                        #else
                            inputData[blockIndex][inputIndex].x = realXXPol;
                            inputData[blockIndex][inputIndex].y = imagXXPol;
                            inputData[blockIndex][inputIndex+1].x = 0;
                            inputData[blockIndex][inputIndex+1].y = 0;
                            inputData[blockIndex][inputIndex+2].x = 0;
                            inputData[blockIndex][inputIndex+2].y = 0;
                            inputData[blockIndex][inputIndex+1].x = realYYPol;
                            inputData[blockIndex][inputIndex+1].y = imagYYPol;
                        #endif
                    #endif
                #else
                    unsigned int inputIndex = contiguousIndex*numInputVisibilities + i;
                    #ifdef INPUT16BIT
                        #ifdef INPUTINTEGER
                            short realPolAsShort = (short)((realXXPol + realYYPol)*(1<<15));
                            short imagPolAsShort = (short)((imagXXPol + imagYYPol)*(1<<15));
                            inputData[blockIndex][inputIndex].x = *reinterpret_cast<half *>(&realPolAsShort);
                            inputData[blockIndex][inputIndex].y = *reinterpret_cast<half *>(&imagPolAsShort);
                        #else
                            inputData[blockIndex][inputIndex].x = (half)(realXXPol + realYYPol);
                            inputData[blockIndex][inputIndex].y = (half)(imagXXPol + imagYYPol);
                        #endif
                    #else
                        #ifdef INPUTINTEGER
                            int realPolAsInt = (int)((realXXPol + realYYPol)*(1<<31);
                            int imagPolAsInt = (int)((imagXXPol + imagYYPol)*(1<<31);
                            inputData[blockIndex][inputIndex].x = *reinterpret_cast<float *>(&realPolAsInt);
                            inputData[blockIndex][inputIndex].y = *reinterpret_cast<float *>(&imagPolAsInt);
                        #else
                            inputData[blockIndex][inputIndex].x = realXXPol + realYYPol;
                            inputData[blockIndex][inputIndex].y = imagXXPol + imagYYPol;
                        #endif
                    #endif
                #endif
            }
        }
    }
    setAllMemoryRegionsPopulated(manager, true);
    logger(LOG_INFO, "Metric Generator test data has been generated");

    char *rdmaDeviceNameChars = NULL; /* no preferred rdma device name to choose */
    if (!rdmaDeviceName.empty())
        rdmaDeviceNameChars = const_cast<char*>(rdmaDeviceName.c_str());
    char *identifierFileNameChars = NULL; /* default to using stdio for exchanging RDMA identifiers */
    if (!identifierFileName.empty())
        identifierFileNameChars = const_cast<char*>(identifierFileName.c_str());
    char *metricURL = NULL; /* default to not push metrics */
    uint32_t numMetricAveraging = 0; /* number of message completions over which to average metrics, default to numMemoryBlocks*numContiguousMessages */
    /* create a pointer to an identifier exchange function (see eg RDMAexchangeidentifiers for some examples) */
    enum exchangeResult (*identifierExchangeFunction)(bool isSendMode,
        uint32_t packetSequenceNumber, uint32_t queuePairNumber, union ibv_gid gidAddress, uint16_t localIdentifier,
        uint32_t *remotePSNPtr, uint32_t *remoteQPNPtr, union ibv_gid *remoteGIDPtr, uint16_t *remoteLIDPtr) = NULL;
    if (identifierFileNameChars != NULL)
    {
        setIdentifierFileName(identifierFileNameChars);
        identifierExchangeFunction = exchangeViaSharedFiles;
    }

    /* perform the RDMA transfers */
    rdmaTransferMemoryBlocks(SEND_MODE, manager,
        messageDelayTime, identifierExchangeFunction,
        rdmaDeviceNameChars, rdmaPort, gidIndex,
        metricURL, numMetricAveraging);
    waitForRdmaTransferCompletion(manager); 

    /* free memory block buffers */
    destroyMemoryRegionManager(manager);
    for (unsigned int blockIndex=0; blockIndex<numMemoryBlocks; blockIndex++)
    {
        free(inputData[blockIndex]);
    }

    std::cout << "Simulated Visibility Generator ending" << std::endl;
    return 0;
}