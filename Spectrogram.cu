// Copyright 2020 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * Spectrogram.cu
 * Andrew Ensor
 * C++/CUDA program for generating spectrogram for complex 16 or 32 bit data received in memory blocks as either FP16/float or short/int
 * Note this prototype is explicitly managing host<->device memory transfers rather than using cudaMallocManaged unified memory
 * Build with: nvcc FFTWindow.cu Spectrogram.cu -lrdmaapi -lrdmacm -libverbs -lcurl -lpthread -lcufft -lm -arch=sm_60 -Xptxas -O3 -o spectrogram
 * Without RDMA api library: nvcc FFTWindow.cu Spectrogram.cu ../rdma-data-transport/RDMAapi.c ../rdma-data-transport/RDMAmetricgenerator.c ../rdma-data-transport/RDMAmemorymanager.c ../rdma-data-transport/RDMAexchangeidcallbacks.c ../rdma-data-transport/RDMAlogger.c -lrdmacm -libverbs -lcurl -lpthread -lcufft -lm  -arch=sm_60 -Xptxas -O3 -o spectrogram
 * Run with: ./spectrogram -m65536 -b8192 -v1024
 * Note -arch is specified so can use half2 arithmetic
 * Note -Xptxas -O3 are for compiled kernel code and host code optimizations
 * Note this project uses RDMA as a static library called rdmaapi.a
*/

#include "Spectrogram.h"

/**********************************************************************
 * convertIntToFloatOnGPU Cuda kernel that converts int values to float
 * values in-place in the deviceData
 **********************************************************************/
#ifdef INPUT16BIT
    __global__ void convertIntToFloatOnGPU(half2 *deviceData, int n)
#else
    __global__ void convertIntToFloatOnGPU(cufftComplex *deviceData, int n)
#endif
{
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    if (i < n)
    {
        #ifdef DUALPOLARISED
            #ifdef INPUT16BIT
                short realXComponent = __half_as_short(deviceData[2*i].x);
                short imagXComponent = __half_as_short(deviceData[2*i].y);
                deviceData[2*i].x = __hdiv((half)realXComponent, (half)(1<<15));
                deviceData[2*i].y = __hdiv((half)imagXComponent, (half)(1<<15));
                short realYComponent = __half_as_short(deviceData[2*i+1].x);
                short imagYComponent = __half_as_short(deviceData[2*i+1].y);
                deviceData[2*i+1].x = __hdiv((half)realYComponent, (half)(1<<15));
                deviceData[2*i+1].y = __hdiv((half)imagYComponent, (half)(1<<15));
            #else
                int realXComponent = __float_as_int(deviceData[2*i].x);
                int imagXComponent = __float_as_int(deviceData[2*i].y);
                deviceData[2*i].x = ((float)realXComponent) / (1<<31);
                deviceData[2*i].y = ((float)imagXComponent) / (1<<31);
                int realYComponent = __float_as_int(deviceData[2*i+1].x);
                int imagYComponent = __float_as_int(deviceData[2*i+1].y);
                deviceData[2*i+1].x = ((float)realYComponent) / (1<<31);
                deviceData[2*i+1].y = ((float)imagYComponent) / (1<<31);
            #endif
        #else
            #ifdef INPUT16BIT
                short realComponent = __half_as_short(deviceData[i].x);
                short imagComponent = __half_as_short(deviceData[i].y);
                deviceData[i].x = __hdiv((half)realComponent, (half)(1<<15));
                deviceData[i].y = __hdiv((half)imagComponent, (half)(1<<15));
            #else
                int realComponent = __float_as_int(deviceData[i].x);
                int imagComponent = __float_as_int(deviceData[i].y);
                deviceData[i].x = ((float)realComponent) / (1<<31);
                deviceData[i].y = ((float)imagComponent) / (1<<31);
            #endif
        #endif
    }
}


/**********************************************************************
 * windowOnGPU Cuda kernel that multiplies each complex deviceData
 * entry in place by its scalar deviceWindow entry
 **********************************************************************/
#ifdef INPUT16BIT
    __global__ void windowOnGPU(half *deviceWindow, half2 *deviceData, int n)
#else
    __global__ void windowOnGPU(float *deviceWindow, cufftComplex *deviceData, int n)
#endif
{
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    if (i < n)
    {
        #ifdef DUALPOLARISED
            #ifdef INPUT16BIT
                deviceData[2*i] = __hmul2(deviceData[2*i], __half2half2(deviceWindow[i]));
                deviceData[2*i+1] = __hmul2(deviceData[2*i+1], __half2half2(deviceWindow[i]));
            #else
                deviceData[2*i] = cuCmulf(deviceData[2*i], make_cuComplex(deviceWindow[i],0));
                deviceData[2*i+1] = cuCmulf(deviceData[2*i+1], make_cuComplex(deviceWindow[i],0));
            #endif
        #else
            #ifdef INPUT16BIT
                deviceData[i] = __hmul2(deviceData[i], __half2half2(deviceWindow[i]));
            #else
                deviceData[i] = cuCmulf(deviceData[i], make_cuComplex(deviceWindow[i],0));
            #endif
        #endif
    }
}


/**********************************************************************
 * sumPowerOnGPU Cuda kernel that converts complex deviceData entries
 * to float power values and adds them to deviceSum
 **********************************************************************/
#ifdef INPUT16BIT
    __global__ void sumPowerOnGPU(half2 *deviceData, float *deviceSum, int n)
#else
    __global__ void sumPowerOnGPU(cufftComplex *deviceData, float *deviceSum, int n)
#endif
{
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    if (i < n)
    {
        #ifdef DUALPOLARISED
            #ifdef INPUT16BIT
                cufftComplex xPol = __half22float2(deviceData[2*i]);
                cufftComplex yPol = __half22float2(deviceData[2*i+1]);
            #else
                cufftComplex xPol = deviceData[2*i];
                cufftComplex yPol = deviceData[2*i+1];
            #endif
            deviceSum[4*i] += xPol.x*xPol.x + xPol.y*xPol.y;
            deviceSum[4*i+1] += yPol.x*yPol.x + yPol.y*yPol.y;
            cufftComplex crossPower; // equals conj(xPol)*yPol
            crossPower.x = xPol.x*yPol.x + xPol.y*yPol.y;
            crossPower.y = xPol.x*yPol.y - xPol.y*yPol.x;
            deviceSum[4*i+2] += crossPower.x;
            deviceSum[4*i+3] += crossPower.y;
        #else
            #ifdef INPUT16BIT
                deviceSum[i] += __half2float(deviceData[i].x)*__half2float(deviceData[i].x)
                    + __half2float(deviceData[i].y)*__half2float(deviceData[i].y); // immediately promote to FP32 to reduce overflow risks
            #else
                deviceSum[i] += deviceData[i].x*deviceData[i].x + deviceData[i].y*deviceData[i].y;
            #endif
        #endif
    }
}


/**********************************************************************
 * averageonGPU Cuda kernel that divides each entry in deviceSum by numTerms
 **********************************************************************/
__global__ void averageOnGPU(float *deviceSum, int n, uint numTerms)
{
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    if (i < n)
    {
        #ifdef DUALPOLARISED
            deviceSum[4*i] /= numTerms;
            deviceSum[4*i+1] /= numTerms;
            deviceSum[4*i+2] /= numTerms;
            deviceSum[4*i+3] /= numTerms;
        #else
            deviceSum[i] /= numTerms;
        #endif
    }
}


/**********************************************************************
 * Checks the Cuda status and logs any errors
 **********************************************************************/
cudaError_t Spectrogram::checkCudaStatus()
{
    cudaError_t errorSynch = cudaGetLastError();
    cudaError_t errorAsync = cudaDeviceSynchronize(); // blocks host thread until all previously issued CUDA commands have completed
    if (errorSynch != cudaSuccess)
        logger(LOG_NOTICE, "Cuda synchronous error %s", cudaGetErrorString(errorSynch));
    if (errorAsync != cudaSuccess)
        logger(LOG_NOTICE, "Cuda asynchronous error %s", cudaGetErrorString(errorAsync));
    if (errorSynch != cudaSuccess)
        return errorSynch;
    else
        return errorAsync;
}


/**********************************************************************
 * Spectrogram default constructor
 **********************************************************************/
Spectrogram::Spectrogram(std::string databaseURL, std::string outputName)
{
    this->databaseURL = databaseURL;
    this->outputName = outputName;
}


/**********************************************************************
 * Spectrogram init to set up working data buffers and cuFFT plan
 **********************************************************************/
void Spectrogram::init(uint numInputSamples, FFTWindow *window)
{
    this->numInputSamples = numInputSamples;
    this->window = window;

    // output GPU device properties
    int numGPUDevices = 0;
    cudaGetDeviceCount(&numGPUDevices);
    for (int i=0; i<numGPUDevices; i++)
    {
        cudaDeviceProp foundDeviceProperties;
        cudaGetDeviceProperties(&foundDeviceProperties, i);
        if (i == 0)
            this->gpuDeviceProperties = foundDeviceProperties;
        double peakMemoryBandwidth = 2.0 * foundDeviceProperties.memoryClockRate
            * foundDeviceProperties.memoryBusWidth/1.0E6;
        logger(LOG_NOTICE, "GPU device name %s, memory clock rate %dkHz,"
            " memory bus width %dbits, peak memory bandwidth %fGbps",
            foundDeviceProperties.name, foundDeviceProperties.memoryClockRate,
            foundDeviceProperties.memoryBusWidth, peakMemoryBandwidth);
    }

    // calculate suitable cuda blockSize and cuda gridSize
    // following https://developer.nvidia.com/blog/cuda-pro-tip-occupancy-api-simplifies-launch-configuration/
    int minGridSize;
    cudaOccupancyMaxPotentialBlockSize(&minGridSize, &this->cudaBlockSize, sumPowerOnGPU, 0, 0);
    this->cudaGridSize = (this->numInputSamples+this->cudaBlockSize-1)/this->cudaBlockSize;   
    logger(LOG_NOTICE, "Block size %d, minimum grid size %d, chosen grid size %d",
        this->cudaBlockSize, minGridSize, this->cudaGridSize);

    // allocate the device transfer buffer for input data block
    #ifdef INPUT16BIT
        size_t numBlockBytes = this->numInputSamples*sizeof(half2);
    #else
        size_t numBlockBytes = this->numInputSamples*sizeof(cufftComplex);
    #endif
    #ifdef DUALPOLARISED
        numBlockBytes *= 2;
    #else
        numBlockBytes *= 1;
    #endif
    cudaMalloc((void**)&deviceData, numBlockBytes);
    // allocate the device transfer buffer for window
    if (window != NULL)
    {
        #ifdef INPUT16BIT
            size_t numWindowBytes = this->numInputSamples*sizeof(half); // window has only scalar (not complex) values
        #else
            size_t numWindowBytes = this->numInputSamples*sizeof(float);
        #endif
        cudaMalloc((void**)&deviceWindow, numWindowBytes);
        // transfer the window data to device
        cudaMemcpy(deviceWindow, window->getEntries(), numWindowBytes, cudaMemcpyHostToDevice);
        logger(LOG_INFO, "Spectrogram operating with windowing");
    }
    else
    {
        logger(LOG_INFO, "Spectrogram operating without windowing");
    }

    // allocate the buffer for the sums on the GPU device
    size_t numSumBytes = this->numInputSamples*sizeof(float); // for single polarisation
    #ifdef DUALPOLARISED
        numSumBytes *= 4; // store power for each polarisation and cross-power real and imaginary components
    #else
        numSumBytes *= 1; // store power for single polarisation
    #endif
    cudaMalloc((void**)&this->deviceSum, numSumBytes);
    cudaMemset(this->deviceSum, 0, numSumBytes); // clear the sums to zero

    // create the cuFFT plan
    #ifdef DUALPOLARISED
        long long int fftDim[1] = {this->numInputSamples};
        size_t workSize = 0;
        const int batch = 2;
        if (cufftCreate(&plan) != CUFFT_SUCCESS)
        {
            logger(LOG_ERR, "CUFFT error failed to create plan");
        }
        #ifdef INPUT16BIT // NOTE may want to check whether CUDA_C_16F internal operations introduce any artifacts
            if (cufftXtMakePlanMany(plan, 1, fftDim, fftDim, 2, 1, CUDA_C_16F, fftDim, 2, 1, CUDA_C_16F,
                batch, &workSize, CUDA_C_16F) != CUFFT_SUCCESS)
            {
                logger(LOG_ERR, "CUFFT error failed to configure plan many for FP16");
            }
        #else
            if (cufftXtMakePlanMany(plan, 1, fftDim, fftDim, 2, 1, CUDA_C_32F, fftDim, 2, 1, CUDA_C_32F,
                batch, &workSize, CUDA_C_32F) != CUFFT_SUCCESS)
            {
                logger(LOG_ERR, "CUFFT error failed to configure plan many for FP32");
            }
        #endif
    #else
        #ifdef INPUT16BIT
            long long int fftDim[1] = {this->numInputSamples};
            size_t workSize = 0;
            if (cufftCreate(&plan) != CUFFT_SUCCESS)
            {
                logger(LOG_ERR, "CUFFT error failed to create plan");
            }
            if (cufftXtMakePlanMany(plan, 1, fftDim, NULL, 1, 1, CUDA_C_16F, NULL, 1, 1, CUDA_C_16F,
                1, &workSize, CUDA_C_16F) != CUFFT_SUCCESS)
            {
                logger(LOG_ERR, "CUFFT error failed to configure plan many for FP16");
            }
        #else
            if (cufftPlan1d(&plan, this->numInputSamples, CUFFT_C2C, 1) != CUFFT_SUCCESS)
            {
                logger(LOG_ERR, "CUFFT error failed to configure plan 1d");
            }
        #endif
    #endif

    // initialise the libcurl library for the posting of data to InfluxDB
    curl_global_init(CURL_GLOBAL_ALL);
}


/**********************************************************************
 * Spectrogram accumulateFFT to transfer input block, perform FFT and accumulate
 **********************************************************************/
void Spectrogram::accumulateFFT(void* dataPointer)
{
    // transfer the input block to the device
    #ifdef INPUT16BIT
        half2 *inputBlock = (half2 *)dataPointer;
        size_t numBlockBytes = this->numInputSamples*sizeof(half2);
    #else
        cufftComplex *inputBlock = (cufftComplex *)dataPointer;
        size_t numBlockBytes = this->numInputSamples*sizeof(cufftComplex);
    #endif
    #ifdef DUALPOLARISED
        numBlockBytes *= 2;
    #else
        numBlockBytes *= 1;
    #endif
    cudaMemcpy(deviceData, inputBlock, numBlockBytes, cudaMemcpyHostToDevice);

    #ifdef INPUTINTEGER
        convertIntToFloatOnGPU<<<this->cudaGridSize, this->cudaBlockSize>>>(this->deviceData,
            this->numInputSamples);
    #else
        // default is to accept FP16 or FP32 floating point values so no conversion required
    #endif

    // optionally apply the window to the device data
    if (window != NULL)
    {
        windowOnGPU<<<this->cudaGridSize, this->cudaBlockSize>>>(this->deviceWindow,
            this->deviceData, this->numInputSamples);
    }

    // perform the FFT
    #ifdef INPUT16BIT
        if (cufftXtExec(plan, deviceData, deviceData, CUFFT_FORWARD) != CUFFT_SUCCESS)
        {
            logger(LOG_ERR, "CUFFT error failed to execute FFT");
        }
    #else
        if (cufftExecC2C(plan, deviceData, deviceData, CUFFT_FORWARD) != CUFFT_SUCCESS)
        {
            logger(LOG_ERR, "CUFFT error failed to execute FFT");
        }
    #endif

    // perform the summation of the power of channelised data using FP32 summands
    sumPowerOnGPU<<<this->cudaGridSize, this->cudaBlockSize>>>(this->deviceData,
        this->deviceSum, this->numInputSamples);
}


/**********************************************************************
 * Spectrogram averageAndReturn to average results in deviceData and return to host buffer
 **********************************************************************/
void Spectrogram::averageAndReturn(float *outputData, uint numAverage)
{
    // perform the averaging of the FP32 summands
    averageOnGPU<<<this->cudaGridSize, this->cudaBlockSize>>>(this->deviceSum, this->numInputSamples, numAverage);
    #ifdef DUALPOLARISED
        size_t numSumBytes = this->numInputSamples*sizeof(float)*4; // store power for each polarisation and cross-power real and imaginary components
    #else
        size_t numSumBytes = this->numInputSamples*sizeof(float); // store power for single polarisation
    #endif
    cudaMemcpy(outputData, this->deviceSum, numSumBytes, cudaMemcpyDeviceToHost);
    if (cudaDeviceSynchronize() != cudaSuccess)
    {
        logger(LOG_ERR, "Cuda error failed to synchronise");
    }
    cudaMemset(this->deviceSum, 0, numSumBytes); // clear the sums to zero
}


/**********************************************************************
 * Spectrogram getNumInputSamples returns the number of complex data values per block
 **********************************************************************/
int Spectrogram::getNumInputSamples()
{
    return numInputSamples;
}


/**********************************************************************
 * struct that holds a single POST request of spectrogram data
 **********************************************************************/
struct httpPostParams
{
    pthread_t *postThread;
    std::string databaseURL;
    std::string outputName;
    float *postedData;
    uint startChannel;
    uint endChannel;
    Spectrogram *spectrogram;
};


/**********************************************************************
 * httpPostThread performs the HTTP POST request in a separate pthread  
 **********************************************************************/
static void *httpPostThread(void *ptr)
{
    struct httpPostParams *params = (struct httpPostParams *)ptr;
    // set the post header information for InfluxDB
    std::string postRequestHeader = "http://" + params->databaseURL
        + ":8086/write?db=spectrogram&precision=ms";
    unsigned long long millisecondsSinceEpoch
        = std::chrono::system_clock::now().time_since_epoch() / std::chrono::milliseconds(1);
    // set the post body information for InfluxDB
    std::stringstream postRequestBody;
    for (unsigned int i=params->startChannel; i<params->endChannel; i++)
    {
        if (i > params->startChannel)
            postRequestBody << std::endl;
        postRequestBody << params->outputName << ",ch=" << std::setw(6) << std::setfill('0') << i;
        #ifdef DUALPOLARISED
            postRequestBody << " xpow=" << params->postedData[4*i]
                << ",ypow=" << params->postedData[4*i+1]
                << ",cpowr=" << params->postedData[4*i+2]
                << ",cpowi=" << params->postedData[4*i+3]
                << " " << millisecondsSinceEpoch;
        #else
            postRequestBody << " pow=" << params->postedData[i] << " " << millisecondsSinceEpoch;
        #endif
    }
    // send the post request using curl
    CURL *curl;
    CURLcode res;
    curl = curl_easy_init();
    if (curl != NULL)
    {
        curl_easy_setopt(curl, CURLOPT_URL, postRequestHeader.c_str()); // note libcurl expects char* rather than std::string
        const std::string postRequestBodyString(postRequestBody.str()); // note stringstream str returns only a temporary string
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postRequestBodyString.c_str());
        res = curl_easy_perform(curl);
        if (res != CURLE_OK)
            logger(LOG_WARNING, "Unable to post metrics via HTTP with curl error %s", curl_easy_strerror(res));
        // log the data posted to InfluxDB, note this needs to works fine even after spectrogram object destroyed
        logger(LOG_DEBUG, "HTTP Data Posted to InfluxDB: %s\n%s", postRequestHeader.c_str(), postRequestBodyString.c_str());
        curl_easy_cleanup(curl);
    }
    free(params->postedData); // corresponding to malloc in postDataToDatabase
    free(params->postThread); // corresponding to malloc in postDataToDatabase
    delete params; // corresponding to new in postDataToDatabase
    return NULL;
}


/**********************************************************************
 * Spectrogram postDataToDatabase posts the data block to a database via an HTTP request  
 **********************************************************************/
void Spectrogram::postDataToDatabase(float *outputData, uint startChannel, uint endChannel)
{
    // copy the output data to a temporary memory block which will be available later for pthread
    // note don't use cudaHostAlloc for this as that would require cudaFreeHost in pthread (so cuda calls across multiple threads)
    float *postedData = NULL;
    #ifdef DUALPOLARISED
        size_t numPostBytes = this->numInputSamples*sizeof(float)*4; // store power for each polarisation and cross-power real and imaginary components

    #else
        size_t numPostBytes = this->numInputSamples*sizeof(float); // store power for single polarisation
    #endif
    postedData = (float*)malloc(numPostBytes); // free is called in httpPostThread
    memcpy(postedData, outputData, numPostBytes);

    // post the data to the time series database using a new pthread
    pthread_t *postThread = NULL;
    postThread = (pthread_t*)malloc(sizeof(pthread_t)); // free is called in httpPostThread
    pthread_attr_t pthreadAttributes;
    pthread_attr_init(&pthreadAttributes);
    pthread_attr_setstacksize(&pthreadAttributes, 65536); // 64k stack allocation for pthread
    pthread_attr_setdetachstate(&pthreadAttributes, PTHREAD_CREATE_DETACHED);

    httpPostParams *params = new httpPostParams; // delete is called in httpPostThread
    params->postThread = postThread;
    params->databaseURL = this->databaseURL;
    params->outputName = this->outputName;
    params->postedData = postedData;
    params->startChannel = std::max((uint)0,startChannel);
    params->endChannel = std::min((uint)this->numInputSamples, endChannel);
    params->spectrogram = this;
    if (pthread_create(postThread, &pthreadAttributes, &httpPostThread, (void*)params) != 0)
        logger(LOG_ERR, "Unable to create thread for posting metrics via HTTP");
}


/**********************************************************************
 * Spectrogram cleanupBuffers to free the memory buffers
 **********************************************************************/
void Spectrogram::cleanupBuffers()
{
    cudaDeviceSynchronize();
    int maxActiveBlocks;
    cudaOccupancyMaxActiveBlocksPerMultiprocessor(&maxActiveBlocks, sumPowerOnGPU, this->cudaBlockSize, 0);
    cudaGetDeviceProperties(&this->gpuDeviceProperties, 0);
    float occupancy = (maxActiveBlocks*this->cudaBlockSize/this->gpuDeviceProperties.warpSize)
        / (float)(this->gpuDeviceProperties.maxThreadsPerMultiProcessor/this->gpuDeviceProperties.warpSize);
    logger(LOG_NOTICE, "Maximum active blocks %d, occupancy %f", maxActiveBlocks, occupancy);
    cufftDestroy(this->plan);
    cudaFree(this->deviceData);
    cudaFree(this->deviceSum);
    // cleanup the libcurl library for the posting of data to InfluxDB
    curl_global_cleanup();
}


/**********************************************************************
 * parseCommandLineArguments parses the command line arguments for configuring the spectrogram
 **********************************************************************/
void parseCommandLineArguments(int argc, char *argv[], enum logType &requestLevel,
    uint32_t &messageSize, uint32_t &numMemoryBlocks, uint32_t &numContiguousMessages, uint64_t &numTotalMessages,
    std::string &rdmaDeviceName, uint8_t &rdmaPort, int &gidIndex, std::string &identifierFileName,
    std::string &outputURL, uint32_t &numOutputAveraging, std::string &outputName, FFTWindow::WindowType &requestWindow,
    uint32_t &startChannel, uint32_t &endChannel)
{
    int opt;
    while ((opt = getopt(argc, argv, "l:m:b:c:t:r:p:g:x:o:v:n:w:s:e:")) != -1)
    {
        switch (opt)
        {
            case 'l' :
                if (optarg != NULL)
                {
                    uint32_t level = strtoul(optarg, NULL, 10);
                    if (level > static_cast<int>(LOG_DEBUG))
                        requestLevel = LOG_DEBUG;
                    else
                        requestLevel = static_cast<logType>(level);
                    logger(LOG_DEBUG, "Command line argument: log level %d", requestLevel);
                }
                break;
            case 'm' :
                if (optarg != NULL)
                    messageSize = strtoul(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: messageSize %" PRIu32, messageSize);
                break;
            case 'b' :
                if (optarg != NULL)
                    numMemoryBlocks = strtoul(optarg, NULL, 10);
                if (numMemoryBlocks < 1)
                    numMemoryBlocks = 1;
                logger(LOG_DEBUG, "Command line argument: numMemoryBlocks %" PRIu32, numMemoryBlocks);
                break;
            case 'c' :
                if (optarg != NULL)
                    numContiguousMessages = strtoul(optarg, NULL, 10);
                if (numContiguousMessages < 1)
                    numContiguousMessages = 1;
                logger(LOG_DEBUG, "Command line argument: numContiguousMessages %" PRIu32, numContiguousMessages);
                break;
            case 't' :
                if (optarg != NULL)
                    numTotalMessages = strtoull(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: numTotalMessages %" PRIu64, numTotalMessages);
                break;
            case 'r' :
                if (optarg != NULL)
                    rdmaDeviceName = optarg;
                logger(LOG_DEBUG, "Command line argument: rdmaDevice %s", rdmaDeviceName.c_str());
                break;
            case 'p' :
                if (optarg != NULL)
                    rdmaPort = strtoul(optarg, NULL, 10);
                if (rdmaPort < 1)
                    rdmaPort = 1;
                else if (rdmaPort > 2)
                    rdmaPort = 2;
                logger(LOG_DEBUG, "Command line argument: rdmaPort %d", rdmaPort);
                break;
            case 'g' :
                if (optarg != NULL)
                    gidIndex = strtol(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: gidIndex %d", gidIndex);
                break;
            case 'x' :
                if (optarg != NULL)
                    identifierFileName = optarg;
                logger(LOG_DEBUG, "Command line argument: identifierFileName %s", identifierFileName.c_str());
                break;
            case 'o' :
                if (optarg != NULL)
                    outputURL = optarg;
                logger(LOG_DEBUG, "Command line argument: outputURL %s", outputURL.c_str());
                break;
            case 'v' :
                if (optarg != NULL)
                    numOutputAveraging = strtoul(optarg, NULL, 10);
                if (numOutputAveraging < 1)
                    numOutputAveraging = 1;
                logger(LOG_DEBUG, "Command line argument: numOutputAveraging %" PRIu32, numOutputAveraging);
                break;
            case 'n' :
                if (optarg != NULL)
                    outputName = optarg;
                logger(LOG_DEBUG, "Command line argument: outputName %s", outputName.c_str());
                break;
            case 'w' :
                if (optarg != NULL)
                {
                    uint32_t level = strtoul(optarg, NULL, 10);
                    if (level > int(FFTWindow::WindowType::KAISER))
                        requestWindow = FFTWindow::WindowType::KAISER;
                    else
                        requestWindow = (FFTWindow::WindowType)level;
                    logger(LOG_DEBUG, "Command line argument: window %d", requestWindow);
                }
                break;
            case 's' :
                if (optarg != NULL)
                    startChannel = strtoul(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: startChannel %" PRIu32, startChannel);
                break;
            case 'e' :
                if (optarg != NULL)
                    endChannel = strtoul(optarg, NULL, 10);
                logger(LOG_DEBUG, "Command line argument: endChannel %" PRIu32, endChannel);
                break;
            default :
                  std::cout << "Invalid command line argument" << std::endl;
                logger(LOG_CRIT, "Usage: %s [-l log level 0..6]"
                    "[-m message size in bytes] [-b num memory blocks] [-c num contig messages per block] "
                    "[-t total num messages] "
                    "[-r rdma device name] [-p device port] [-g gid index] [-x exchange identifier filename] "
                    "[-o output URL] [-v output averaging] [-n output name] "
                    "[-w window 0..6] [s start channel] [e end channel]",
                    argv[0]);
        }
    }
}


/**********************************************************************
 * Main method to execute
 **********************************************************************/
int main(int argc, char *argv[])
{
    std::cout << "Spectrogram starting";
    #ifdef INPUT16BIT
        #ifdef INPUTINTEGER
            std::cout << " for short precision complex input data";
        #else
            std::cout << " for half precision complex input data";
        #endif
    #else
        #ifdef INPUTINTEGER
            std::cout << " for int precision complex input data";
        #else
            std::cout << " for float precision complex input data";
        #endif
    #endif
    #ifdef DUALPOLARISED
        std::cout << " with dual polarisations" << std::endl;
    #else
        std::cout << " with single polarisation" << std::endl;
    #endif

    enum logType requestLogLevel = LOG_NOTICE;
    uint32_t messageSize = 65536; /* size in bytes of single RDMA message */
    uint32_t numMemoryBlocks = 1; /* number of memory blocks to allocate for RDMA messages */
    uint32_t numContiguousMessages = 1; /* number of contiguous messages to hold in each memory region */
    uint64_t numTotalMessages = 0; /* total number of messages to send or receive, if 0 then default to numMemoryBlocks*numContiguousMessages */
    std::string rdmaDeviceName; /* no preferred rdma device name to choose */
    uint8_t rdmaPort = 1;
    int gidIndex = -1; /* preferred gid index or -1 for no preference */
    std::string identifierFileName; /* default to using stdio for exchanging RDMA identifiers */
    std::string outputURL = "156.62.60.71"; /* default URL to which to post channelised power data */
    uint32_t numOutputAveraging = 0; /* number of input blocks to average in frequency domain for each output block, if 0 then default to numContiguousMessages */
    std::string outputName = "spectest";
    FFTWindow::WindowType requestWindow = FFTWindow::WindowType::NONE;
    uint32_t startChannel = 0; // lowest channel to output
    uint32_t endChannel = 0;

    parseCommandLineArguments(argc, argv, requestLogLevel,
        messageSize, numMemoryBlocks, numContiguousMessages, numTotalMessages,
        rdmaDeviceName, rdmaPort, gidIndex, identifierFileName,
        outputURL, numOutputAveraging, outputName,
        requestWindow, startChannel, endChannel);
    if (numTotalMessages == 0)
        numTotalMessages = numMemoryBlocks * numContiguousMessages;
    if (numOutputAveraging == 0)
        numOutputAveraging = numContiguousMessages;
    if (endChannel == 0)
        endChannel = std::min(messageSize, (uint32_t)512); // highest+1 channel to output
    setLogLevel(requestLogLevel);

// TODO ALLOW MULTIPLE DATABLOCKS PER MESSAGE
    #ifdef DUALPOLARISED
        #ifdef INPUT16BIT
            const uint numInputSamples = (messageSize/2)/sizeof(half2);
        #else
            const uint numInputSamples = (messageSize/2)/sizeof(cufftComplex);
        #endif
    #else
        #ifdef INPUT16BIT
            const uint numInputSamples = messageSize/sizeof(half2);
        #else
            const uint numInputSamples = messageSize/sizeof(cufftComplex);
        #endif
    #endif

    FFTWindow *window = NULL;
    if (requestWindow != FFTWindow::WindowType::NONE)
    {
        window = new FFTWindow(requestWindow);
        window->init(numInputSamples);
    }

    Spectrogram spectrogram = Spectrogram(outputURL, outputName);
    spectrogram.init(numInputSamples, window);

    /* allocate page-locked device-accessible memory blocks as buffers to be used in the memory regions */
    #ifdef INPUT16BIT
        size_t numInputBytes = spectrogram.getNumInputSamples()*sizeof(half2);
        half2 *inputData[numMemoryBlocks];
    #else
        size_t numInputBytes = spectrogram.getNumInputSamples()*sizeof(cufftComplex);
        cufftComplex *inputData[numMemoryBlocks];
    #endif
    #ifdef DUALPOLARISED
        numInputBytes *= 2;
    #else
        numInputBytes *= 1;
    #endif
    for (unsigned int blockIndex=0; blockIndex<numMemoryBlocks; blockIndex++)
    {
        cudaHostAlloc(&inputData[blockIndex], numInputBytes*numContiguousMessages, 0); // Note including cudaHostAllocWriteCombined flag fails to register for RDMA
    }

    /* create a memory region manager to manage the usage of the memory blocks */
    /* note for simplicity memory region manager doesn't monitor memory regions so this code doesn't have to track sending/receiving */
    MemoryRegionManager* manager = createMemoryRegionManager((void **)inputData,
        messageSize, numMemoryBlocks, numContiguousMessages, numTotalMessages, false);
    setAllMemoryRegionsPopulated(manager, false);

    // allocate the output buffer which will hold the results
    float *outputData;
    size_t numOutputBytes = spectrogram.getNumInputSamples()*sizeof(float); // for single polarisation
    #ifdef DUALPOLARISED
        numOutputBytes *= 4; // store power for each polarisation and cross-power real and imaginary components
    #else
        numOutputBytes *= 1; // store power for single polarisation
    #endif
    cudaHostAlloc(&outputData, numOutputBytes, 0); // store power for each polarisation and cross-power
    spectrogram.checkCudaStatus();

    /* set up timers */
    cudaEvent_t startTime, stopTime;
    cudaEventCreate(&startTime);
    cudaEventCreate(&stopTime);
    float executionTime = 0; // milliseconds
    cudaEventRecord(startTime);

    char *rdmaDeviceNameChars = NULL; /* no preferred rdma device name to choose */
    if (!rdmaDeviceName.empty())
        rdmaDeviceNameChars = const_cast<char*>(rdmaDeviceName.c_str());
    char *identifierFileNameChars = NULL; /* default to using stdio for exchanging RDMA identifiers */
    if (!identifierFileName.empty())
        identifierFileNameChars = const_cast<char*>(identifierFileName.c_str());
    char *metricURL = NULL; /* default to not push metrics */
    uint32_t numMetricAveraging = 0; /* number of message completions over which to average metrics, default to numMemoryBlocks*numContiguousMessages */

    /* create a pointer to an identifier exchange function (see eg RDMAexchangeidentifiers for some examples) */
    enum exchangeResult (*identifierExchangeFunction)(bool isSendMode,
        uint32_t packetSequenceNumber, uint32_t queuePairNumber, union ibv_gid gidAddress, uint16_t localIdentifier,
        uint32_t *remotePSNPtr, uint32_t *remoteQPNPtr, union ibv_gid *remoteGIDPtr, uint16_t *remoteLIDPtr) = NULL;
    if (identifierFileNameChars != NULL)
    {
        setIdentifierFileName(identifierFileNameChars);
        identifierExchangeFunction = exchangeViaSharedFiles;
    }

    /* perform the RDMA transfers */
    rdmaTransferMemoryBlocks(RECV_MODE, manager,
        0, identifierExchangeFunction,
        rdmaDeviceNameChars, rdmaPort, gidIndex,
        metricURL, numMetricAveraging);

    /* process the message data as they get transferred */
    uint64_t expectedTransfer = 0;
    uint32_t currentRegionIndex = 0;
    uint32_t currentContiguousIndex = 0;
    while (expectedTransfer < numTotalMessages)
    {
        /* delay until the current memory location has its data transferred */
        uint64_t currentTransfer = getMessageTransferred(manager, currentRegionIndex, currentContiguousIndex);
        while (currentTransfer==NO_MESSAGE_ORDINAL || currentTransfer<expectedTransfer)
        {
            /* expected message transfer has not yet taken place */
            microSleep(10); /* sleep current thread before checking currentTransfer again */
            currentTransfer = getMessageTransferred(manager, currentRegionIndex, currentContiguousIndex);
        }
        /* process the current message transfer */
        if (currentTransfer > expectedTransfer)
        {
            /* messages between expectedTransfer and currentTransfer-1 inclusive have been dropped during the transfer */
            /* HERE: do something about the missing messages */
            logger(LOG_WARNING, "Dropped messages detected from %lu with %lu messages dropped",
                expectedTransfer, currentTransfer-expectedTransfer);
        }
        #ifdef DUALPOLARISED
            unsigned int inputStartIndex = 2*currentContiguousIndex*spectrogram.getNumInputSamples();
        #else
            unsigned int inputStartIndex = currentContiguousIndex*spectrogram.getNumInputSamples();
        #endif
        spectrogram.accumulateFFT(&inputData[currentRegionIndex][inputStartIndex]);
        if ((currentTransfer+1)%numOutputAveraging == 0)
        {
            logger(LOG_DEBUG, "Performing averaging of datablock summands");
            spectrogram.averageAndReturn(outputData, numOutputAveraging);
            cudaEventRecord(stopTime);
            cudaEventSynchronize(stopTime);
            cudaEventElapsedTime(&executionTime, startTime, stopTime);
            logger(LOG_INFO, "Average execution time is %.2fms", executionTime);
            spectrogram.postDataToDatabase(outputData, startChannel, endChannel);
            cudaEventRecord(startTime);
        }
        /* once done processing the current message transfer move along to the next memory location */
        expectedTransfer = currentTransfer + 1;
        currentContiguousIndex++;
        if (currentContiguousIndex >= manager->numContiguousMessages)
        {
            /* move along to the start of the next memory region */
            setMemoryRegionPopulated(manager, currentRegionIndex, false); /* finished processing this memory region so it can now be reused */
            currentContiguousIndex = 0;
            currentRegionIndex++;
            if (currentRegionIndex >= manager->numMemoryRegions)
            {
                /* wrap around to reuse the same memory regions */
                currentRegionIndex = 0;
            }
        }
    }

    /* ensure the RDMA transfers taking place in a separate thread have completed by making current thread wait until they have completed */
    waitForRdmaTransferCompletion(manager); 

    /* destroy timers */
    cudaEventDestroy(startTime);
    cudaEventDestroy(stopTime);

    /* free memory block buffers */
    cudaFreeHost(outputData);
    destroyMemoryRegionManager(manager);
    for (unsigned int blockIndex=0; blockIndex<numMemoryBlocks; blockIndex++)
    {
        cudaFreeHost(inputData[blockIndex]);
    }
    spectrogram.cleanupBuffers();
    if (window != NULL)
    {
        window->cleanup();
        delete window;
    }

    spectrogram.checkCudaStatus();

    std::cout << "Spectrogram ending" << std::endl;
    return 0;
}